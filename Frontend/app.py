# app.py
from flask import Flask, render_template, request


# Use these import statements for gitlab project structure
import sys, os, time
# sys.path.insert(1, os.getcwd()+'/Backend')
sys.path.insert(1, os.getcwd())
from Backend.backendStarter import test, backend_main, update_layer
import json

# Use these import statements for structure used for deployment
#import sys, time
#path = "./Backend"
#sys.path.append(path)
#from backendStarter import test, backend_main

app = Flask(__name__)

@app.route("/")
def home():
  return render_template("index.html")

@app.route("/index")
def index():
  return render_template("index.html")

@app.route("/about")
def about():
  return render_template("about.html")

# @app.route("/app", methods=["POST", "GET"])
# def get_layer():
#     return json.dumps({"Message":"Hello world"})

@app.route("/app", methods=["POST", "GET"])
def webapp():
  def update_layers(dataDict):
      data = dataDict['DEM']
      returnedStatement = dataDict['statement']
      
      if (data == None):
          print("Error generating DEM, returning message to frontend")
          return render_template("getstarted.html", returnedStatement=returnedStatement)
      template = render_template("app.html",
                                 DEM=data,

                                 trenches_step_val=dataDict['trenches_step']['starting_value'],
                                 trenches_step=dataDict['trenches_step'].get('array'),
                                 trenches_step_bool=bool(dataDict['trenches_step'].get('array')),

                                 trenches_even_val=dataDict['trenches_even']['starting_value'],
                                 trenches_even=dataDict['trenches_even'].get('array'),
                                 trenches_even_bool=bool(dataDict['trenches_even'].get('array')),

                                 ridges_pysheds_val=dataDict['ridges_pysheds']['starting_value'],
                                 ridges_pysheds=dataDict['ridges_pysheds'].get('array'),
                                 ridges_pysheds_bool=bool(dataDict['ridges_pysheds'].get('array')),

                                 gullies_pysheds_val=dataDict['gullies_pysheds']['starting_value'],
                                 gullies_pysheds=dataDict['gullies_pysheds'].get('array'),
                                 gullies_pysheds_bool=bool(dataDict['gullies_pysheds'].get('array')),

                                 aerial=dataDict['aerial'].get('array'),
                                 aerial_bool=bool(dataDict['aerial'].get('array')),

                                 cadastre=dataDict['cadastre'].get('array'),
                                 cadastre_bool=bool(dataDict['cadastre'].get('array')),

                                 # existing_roads=dataDict['existing_roads'],
                                 returnedStatement=dataDict['statement'])
      return template
  if request.method == "POST":
      formDict = request.form.to_dict()
      if not formDict:
          # Extract the parameters manually from the url
          print("Url requested:", request.url)
          parameters = request.url[request.url.find("?")+1:].split("&")
          parameters = [{p.split("=")[0]:p.split("=")[1]} for p in parameters]
          formDict = parameters[0]  # Only handling a single parameter at once for now
      print("Form requested:", formDict)
      if "address" in formDict:
          dataDict = backend_main(request.form['address'])
          return update_layers(dataDict)
      else:
          new_layer = update_layer(formDict)
          return json.dumps(new_layer)

  return render_template("getstarted.html", returnedStatement="")


@app.route("/demos", methods=["POST", "GET"])
def demosapp():
  if request.method == "POST":
    returnedStatement = (test(request.form['address']))
    print ("Demo name entered: " + request.form['address'])
  else:
    returnedStatement = ""
  return render_template("demos.html", returnedStatement=returnedStatement)

@app.route("/<other>")
def hello_name(other):
  return "<h1>Hmm, the page '" + other + "' doesn't seem to exist :(</h1>"

if __name__ == "__main__":
  app.run(debug=False)

