from Backend.queries.querying import generateDEM
import matplotlib.pyplot as plt
import psycopg2

# Need to setup the database locally for this to work

def coord_string_to_tuple(coord):
    return [float(c.strip()) for c in coord.split(',')]

def connect_to_database(user, password, database, host="localhost", port="5432"):
    connection = psycopg2.connect(user=user,
                                  password=password,
                                  database=database,
                                  host=host,
                                  port=port)
    return connection

if __name__ == '__main__':
    connection = connect_to_database(user="me", password="me", database="act", host="localhost", port="5432")

    examples = {
    "spring_valley": "-35.28878, 149.01080",
    "black_mountain" : "-35.270872, 149.099910",
    "swamp_creek" : "-35.2629, 148.9205",
    "mulligans_flat": "-35.1669, 149.1609",
    "rob_roy": "-35.5116, 149.1040",
    "caloola_farm": "-35.6716, 149.0727",
    }

    example = "caloola_farm"
    # for example in examples:
    coords = coord_string_to_tuple(examples[example])

    dem = generateDEM(coords, connection)
    # id_ = generateID(coords, connection)
    # boundary = generateBoundary(coords, connection)
    # address = generateAddress(coords, connection)
    # extent = generateExtent(coords, connection)
    # connection.close()

    # print(dem)
    # print(id_)
    # print(boundary)
    # print(address)
    # print(extent)

    dem = np.array(dem)
    plt.imshow(dem)
    plt.show()
        # np.savetxt(f"../Backend/Data/{example}.txt", dem)

    # scale = (np.max(dem)-np.min(dem))/400
    # print(np.max(dem)-np.min(dem))
    # dem *= scale
    # mlab.surf(dem, colormap='gist_earth')
    # mlab.view()
    # mlab.show()

