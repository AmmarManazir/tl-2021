# Queries
These scripts contain the code necessary to setup and use our own database that we host, as well as code for querying NSW government database. 

## ACT Database Setup
1. brew install postgresql and postgis
2. create a superuser & database
    - `sudo -u postgres psql`
    - `CREATE user me with encrypted password 'me';`
    - `ALTER USER me WITH SUPERUSER;`
    - `CREATE database act;`
    - `exit`
3. Enable PostGIS
    - `psql -U me -d act`
    - `CREATE EXTENSION postgis;`
    - `exit`
4. Load the database
    - Download from https://drive.google.com/file/d/1oq_FEOVMnTxNXTJpgTPTa3TVzIMHGJcw/view?usp=sharing 
    - psql -U me -d act < (filepath/dems.sql)

Should be ready to go

## How dems.sql was created
1. Download the ACT contours (3GB) from [here](https://actmapi-actgov.opendata.arcgis.com/datasets/2015-1m-contours)
    - Click the 'Download as geodatabase in MGA94 zone55'
2. Download the ACT blocks from [here](https://actmapi-actgov.opendata.arcgis.com/datasets/act-blocks)
    - Click 'Download' -> 'Shapefile'
3. Download QGIS from [here](https://www.qgis.org/en/site/forusers/download.html)
4. Use QGIS to convert the ACT contours to a .sql_backup (15GB)
    - Navigate to the file using the browser & double click it (static_contours2015.gbs/contours1m)
    - Right click on the layer that shows up, select Export -> Save Feature As...
    - In Format, choose Postgres SQL dump
    - Choose a file name & location
5. Repeat step 4 for ACT blocks (ACT_Blocks.shp)
6. brew install postgresql and postgis
7. create a superuser & database
    - `sudo -u postgres psql`
    - `CREATE user me with encrypted password 'me';`
    - `ALTER USER me WITH SUPERUSER;`
    - `CREATE database act;`
    - `exit`
8. Enable PostGIS
    - `psql -U me -d act`
    - `CREATE EXTENSION postgis;`
    - `exit`
9. Load the datasets
    - psql -U me -d act < (filepath/act_blocks.sql)
    - psql -U me -d act < (filepath/act_contours.sql)
10. Run the preprocessing.py script
11. Export just the dems table with 
    - `pg_dump  --username me --file "filepath/dems.sql" --table dems act`

## How nsw_dems.sql was created
# TODO: Make all this a bash script so I can just run it
1. Create a database and enable postgis like in ACT database setup
2. shp2pgsql -I -s 7855 Z55/Berridale-CONT-AHD_55_2m.shp new | psql nsw me
    - I have no idea how we obtained these 2m shape files. Ask John.
3. create table contours(wkb_geometry geometry, elevation bigint);
4. INSERT INTO contours(wkb_geometry, elevation) select ST_transform(geom,28355) as wkb_geometry, elevation from new;
5. create table uninterpolated(point geometry,z int);
6. INSERT INTO uninterpolated(point, z)
SELECT ST_SetSRID(ST_POINT(ST_X((dp).geom), ST_Y((dp).geom)), 28355) as point, z FROM 
(SELECT ST_DumpPoints(wkb_geometry) AS dp, elevation AS z 
   FROM contours) as points;
7. CREATE INDEX uninterpolated_idx
  ON uninterpolated
  USING GIST(point);
8. create table interpolated(point geometry,z int);
9. CREATE INDEX berridale_idx
  ON berridale
  USING GIST(point);







select ST_Polygon(ST_MakeLine(ARRAY[p1, 
ST_SetSRID(ST_Point(ST_X(p1), ST_Y(p2)), 28355),  
p2, 
ST_SetSRID(ST_Point(ST_X(p2), ST_Y(p1)), 28355), 
p1]), 28355)
from
(select ST_Transform(ST_SetSRID(ST_Point(148.711146, -36.324460), 4326), 28355) as p1) as foo,
(select ST_Transform(ST_SetSRID(ST_Point(148.781133, -36.282180), 4326), 28355) as p2) as roar;



select ST_SetSRID(ST_Extent(ST_MakeLine(ARRAY[p1, p2, p3, p4, p1])), 28355)
from
(select ST_Transform(ST_SetSRID(ST_Point(148.711146, -36.324460), 4326), 28355) as p1) as foo,
(select ST_Transform(ST_SetSRID(ST_Point(148.711146, -36.282180), 4326), 28355) as p2) as bar,
(select ST_Transform(ST_SetSRID(ST_Point(148.781133, -36.282180), 4326), 28355) as p3) as roar,
(select ST_Transform(ST_SetSRID(ST_Point(148.781133, -36.324460), 4326), 28355) as p4) as nein;