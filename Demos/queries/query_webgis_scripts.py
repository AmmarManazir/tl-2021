import sys, os
sys.path.insert(1, os.getcwd())
import matplotlib.pyplot as plt


#Granitville Estate - -35.055870, 148.990660
    #Home - -35.260910 149.130750
    #Spring Valley Farm - -35.2848 149.0065


def example_image(lat, long):
    Bld_rural_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/LPIMap/MapServer/22/query?geometry="
    ACT_Cadastre_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/basic/MapServer/101/query?geometry="
    NSW_image_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/public/NSW_Imagery/MapServer/export?bbox="
    ACT_Road_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi2020/basic/MapServer/5/query?geometry="
    ACT_image_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/imagery202001mga/ImageServer/exportImage?bbox="
    isACT = check_if_ACT(lat, long)
    box_bound = generate_box_points(lat, long, 0, True)
    prim_bound, sec_bound, cad_ds = generate_cad_points(lat, long, isACT)
    if isACT:
        img = create_stitch_image(prim_bound, ACT_image_url, 0.5)
    else:
        img = create_stitch_image(prim_bound, NSW_image_url, 0.5)
    img_x = img.size[0]
    img_y = img.size[1]
    plt.imshow(img)
    gen_plt_from_url(plt, box_bound, prim_bound, ACT_Cadastre_url, 'orange', '', img.size)
    gen_plt_from_url(plt, prim_bound, prim_bound, ACT_Road_url , 'blue', '', img.size)
    gen_plt_from_url(plt, prim_bound, sec_bound, Bld_rural_url, 'orange', '', img.size)
    ds = generate_entrance(lat, long, ACT_Road_url)
    points_data = np.asarray(extract_points(ds, img_x, img_y, prim_bound))
    #get the centre points (for edge line roads)
    for i in range(len(points_data)):
        for j in range(len(points_data)):
            if not i == j:
                if np.linalg.norm(points_data[i,:]-points_data[j,:]) < 20:
                    new_pt = (points_data[j,:] + points_data[i,:]) / 2
                    points_data[j,:] = new_pt
                    points_data[i,:] = new_pt
    proc_points_data = np.unique(points_data, axis=0)
    gen_plt_from_points(proc_points_data, plt, 'red', '+')
    plt.xlim(0, img_x)
    plt.ylim(img_y, 0)
    plt.show()
    return

def example_Spring_Valley():
    long = 149.01080 #Spring Valley
    lat = -35.28878 #Spring Valley
    #long = 149.0065
    #lat = -35.2848
    #long = 148.789395
    #lat = -35.42861

    NSW_Cadastre_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/Cadastre/MapServer/0/query?geometry="
    ACT_Cadastre_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/basic/MapServer/101/query?geometry="
    Road_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/LPIMap/MapServer/29/query?geometry="
    Contour_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/LPIMap/MapServer/69/query?outFields=elevation&geometry="
    ACT_Contour1m_url= "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/contours_2015/MapServer/2/query?outFields=contour&geometry="
    ACT_Contour5m_url= "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/contours_2015/MapServer/1/query?outFields=contour&geometry="
    ACT_Contour10m_url= "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/contours_2015/MapServer/0/query?outFields=contour&geometry="
    ACT_Contour10m_url2 = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/basic/MapServer/55/query?geometry="
    Bld_rural_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/LPIMap/MapServer/22/query?geometry="
    HydroArea_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/LPIMap/MapServer/46/query?geometry="
    NSW_image_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/public/NSW_Imagery/MapServer/export?bbox="
    ACT_image_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/imagery202001mga/ImageServer/exportImage?bbox="
    ACT_Road_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/basic/MapServer/27/query?geometry="
    ACT_Road_sealed = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi2020/basic/MapServer/4/query?geometry="
    ACT_Road_unsealed = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi2020/basic/MapServer/5/query?geometry="
    ACT_soil_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/soil/MapServer/1/query?geometry="
    ACT_water_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/basic/MapServer/84/query?geometry="


    act_bound, nsw_bound, _ = generate_cad_points(lat, long, True)
    img = create_stitch_image(act_bound, ACT_image_url, 2)
    img_x = img.size[0]
    img_y = img.size[1]
    plt.imshow(img)
    box_bound = generate_box_points(lat, long, 0, True)
    ds = generate_shape(box_bound,ACT_Cadastre_url)
    #ds = generate_shape(act_bound, ACT_Road_unsealed)
    #cad_mask = extract_rasterize_points(ds, img_x, img_y, act_bound)
    #cad_mask = np.where(cad_mask > 0, 1, 0)
    #plt.imshow(ds_raster)
    #print(cad_mask.shape)
    #print(np.max(cad_mask))
    #print(np.min(cad_mask))
    rgb_img = np.array(img)
    #print(rgb_img.shape)
    #print(rgb_img[:,:,2])
    #rgb_img[:,:,2] = rgb_img[:,:,2] * cad_mask

    plt.imshow(rgb_img)
    ds = generate_entrance(lat,long,ACT_Road_unsealed)
    pl
    gen_plt_from_url(plt, box_bound, act_bound, ACT_Cadastre_url, 'red', '', img.size)
    gen_plt_from_url(plt, act_bound, act_bound, ACT_Road_sealed, 'blue', '', img.size)
    gen_plt_from_url(plt, act_bound, act_bound, ACT_Road_unsealed, 'blue', '', img.size)
    plt.xlim(0, img_x)
    plt.ylim(img_y, 0)
    plt.show()


    #generate_shape(nsw_bound, NSW_image_url2)

    #print("test")
    #fig = go.Figure(data=[go.Surface(z=dem)])
    #fig.show()
    #plt.imshow(dem)
    #plt.show()
    #generate_shape_file(act_bound, ACT_Road_url, "ACT_roads")

    #generate_shape_file(generate_box_points(lat,long,0,28355), ACT_Cadastre_url, "ACT_Cadastre")

    #generate_shape_file(nsw_bound, Bld_rural_url, "Building")
    #generate_shape_file(nsw_bound, HydroArea_url, "Water")
    #generate_shape_file(nsw_bound, Road_url, "NSW_Road")
    #generate_shape_file(act_bound, ACT_water_url, "ACT_water")
    #url = generate_image_URL(act_bound, ACT_image_url)
    #print(url)
    #url = "https://maps.six.nsw.gov.au/arcgis/rest/services/public/NSW_Imagery/MapServer/export?bbox=16585806.38%2C-4205194.77%2C16589806.38%2C-4201194.77&format=png&transparent=true&f=image"
    #ret = requests.get(url)
    #img = Image.open(BytesIO(ret.content))
    #plt.imshow(img)
    #plt.show()
    #url2 = "https://maps.six.nsw.gov.au/arcgis/rest/services/public/NSW_Topo_Map/MapServer/export?bbox=16585806.38%2C-4205194.77%2C16589806.38%2C-4201194.77&bboxSR=&layers=0&f=json"
    #url3 = 'http://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/LPIMap/MapServer/69/query?geometry=16585806.38,-4205194.77,16589806.38,-4201194.77&geometryType=esriGeometryEnvelope&f=pjson'
    #url4 = "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/Cadastre/MapServer/0/query?geometry=16585806.38,-4205194.77,16589806.38,-4201194.77&geometryType=esriGeometryEnvelope&f=pjson"
    #"http://maps.six.nsw.gov.au/arcgis/rest/services/public/NSW_Cadastre/MapServer"
    return


if __name__ == '__main__':
    long = 149.01080 #Spring Valley
    lat = -35.28878 #Spring Valley
    #example_Spring_Valley()
    example_image(lat,long)
    #dem = get_cont_from_point(lat, long, 10)
    #np.savetxt("dem.txt", dem)
    #a = RGB_aerial(dem.shape, lat, long)
    #plt.imshow(a)
    #plt.show()

    #dem = get_cont_from_two_points([lat-0.01, long-0.01],[lat+0.01, long+0.01], 5)
    #fig = go.Figure(data=[go.Surface(z=dem)])
    #fig.show()

