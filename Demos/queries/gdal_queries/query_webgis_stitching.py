import sys, os
sys.path.insert(1, os.getcwd())
from Demos.queries.gdal_queries.query_webgis_basic import *
from PIL import Image
import math

ACT_image_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/imagery202001mga/ImageServer/exportImage?bbox="
ACT_image_url_2 = "http://data.actmapi.act.gov.au/arcgis/rest/services/ACT_IMAGERY/imagery202004gda2020/ImageServer/exportImage?bbox="
NSW_image_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/public/NSW_Imagery/MapServer/export?bbox="
NSW_image_url2 = "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/LPI_Imagery_Best/MapServer/export?bbox="

def create_stitch_DEM(bound, feature_url, div):
    """
    :param bound: box coordinates
    :param feature_url: shape elevation
    :return:
    """
    cell_size = 1
    min_x = bound.GetPoint(0)[0]
    min_y = bound.GetPoint(0)[1]
    max_x = bound.GetPoint(1)[0]
    max_y = bound.GetPoint(1)[1]
    pos_x = 0
    pos_y = 0
    step_x = math.ceil(max_x - min_x) / div
    step_y = math.ceil(max_y - min_y) / div
    num_shp_x = div
    num_shp_y = div
    print(step_x, step_y)
    count = 0
    points = np.empty((0,3), np.float)
    while pos_y < num_shp_y:
        while pos_x < num_shp_x:
            sub_bound = generate_bound(min_x + pos_x * step_x, min_y + pos_y*step_y, min_x + (pos_x+1)*step_x, min_y + (pos_y + 1) *step_y)
            sub_ds = generate_shape(sub_bound, feature_url)
            sub_lyr = sub_ds.GetLayer()
            count = count + 1
            for feature in sub_lyr:
                geom = feature.GetGeometryRef()
                z = feature.GetField(0)
                x = geom.Centroid().GetX()
                y = geom.Centroid().GetY()
                points = np.append(points, np.array([[x,y,z]]), axis=0)
            #(num_image_y - pos_y) - flip the y direction (img vs GPS numbering)
            pos_x = pos_x + 1
        pos_x = 0
        pos_y = pos_y + 1
        dimensions = [(max_x - min_x), (max_y - min_y)]
        dimensions = [int(round(e)) for e in dimensions]
        width = int(dimensions[0] / cell_size)
        height = int(dimensions[1] / cell_size)
    return generate_grid(points.astype(np.float), width, height)

def create_stitch_image(bound, feature_url, res):
    """
    :param bound: box coorindates
    :param feature_url: image server to search in
    :param res: resolution of the image in metres/pixel
    :return: combination image of the box boundary
    """
    query_size = 800
    step = int(query_size * res)

    min_x = bound.GetPoint(0)[0]
    min_y = bound.GetPoint(0)[1]
    max_x = bound.GetPoint(1)[0]
    max_y = bound.GetPoint(1)[1]
    pos_x = 0
    pos_y = 0

    num_imgs_x = int(math.ceil(max_x - min_x) / step) + 1
    num_imgs_y = int(math.ceil(max_y - min_y) / step) + 1
    print(num_imgs_x, num_imgs_y)

    pix_x = int((max_x-min_x) / res)
    pix_y = int((max_y-min_y) / res)
    count = 0
    full_img = Image.new('RGB', (pix_x, pix_y))
    print(full_img.size)
    while pos_y < num_imgs_y:
        while pos_x < num_imgs_x:
            sub_bound = generate_bound(min_x + pos_x * step, min_y + pos_y*step, min_x + (pos_x+1)*step, min_y + (pos_y + 1) *step)
            #print(min_x + pos_x * step, min_y + pos_y*step, min_x + (pos_x+1)*step, min_y + (pos_y + 1) *step)
            count = count + 1
            print(count)
            img = generate_image(sub_bound, feature_url, query_size, query_size)
            #(num_image_y - pos_y) - flip the y direction (img vs GPS numbering)
            full_img.paste(img, ((pos_x)*query_size, pix_y - ((pos_y+1)* query_size)))
            pos_x = pos_x + 1
        pos_x = 0
        pos_y = pos_y + 1
    return full_img

def example_generate_images():
    long = 149.01080
    lat = -35.28878
    #lat = -35.260910
    #long = 149.130750
    #lat = -35.055870
    #long = 148.990660
    #lat = -34.591930
    #long = 146.407220
    box_bound, nsw_bound = generate_cad_points(lat, long, True)
    bd = generate_box_points(lat, long, 5000, True)
    #img = generate_image(bd, NSW_image_url, 800, 800)
    print(box_bound.GetPoint(0))
    print(box_bound.GetPoint(1))
    img = create_stitch_image(bd,ACT_image_url_2, 2)
    img.save("test3.png")

    #image.save("winery_1m_2020.png")
    act_bound, nsw_bound = generate_cad_points(lat, long, True)
    #image = create_stitch_image(act_bound,ACT_image_url_2, 0.1)
    #image.save("spring_valley_10cm_2020.jpeg")
    #image = create_stitch_image(act_bound,ACT_image_url_2, 0.2)
    #image.save("spring_valley_20cm_2020.jpeg")
    #image = create_stitch_image(act_bound,ACT_image_url_2, 0.5)
    #image.save("spring_valley_50cm_2020.jpeg")
    #image = create_stitch_image(act_bound,ACT_image_url_2, 1)
    #image.save("spring_valley_1m_2020.jpeg")
    return


