import sys, os

sys.path.insert(1, os.getcwd())
from Demos.queries.gdal_queries.query_webgis_stitching import *

ACT_Cadastre_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/basic/MapServer/101/query?geometry="
NSW_image_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/public/NSW_Imagery/MapServer/export?format=jpg&bbox="
ACT_Road_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi2020/basic/MapServer/5/query?geometry="
ACT_image_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/imagery202001mga/ImageServer/exportImage?bbox="
NSW_Road_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/LPIMap/MapServer/29/query?geometry="

def RGB_aerial(shape, f_lat, f_long):
    isACT = check_if_ACT(f_lat, f_long)
    prim_bound, sec_bound, cad_ds = generate_cad_points(f_lat, f_long, isACT)
    if isACT:
        image_url = ACT_image_url
    else:
        image_url = NSW_image_url
    img = generate_image(prim_bound, image_url, shape[0], shape[1])
    box_bound = generate_box_points(f_lat, f_long, 0, True)
    ds = generate_shape(box_bound, ACT_Cadastre_url)
    # ds = generate_shape(act_bound, ACT_Road_unsealed)
    cad_mask = extract_rasterize_points(ds, shape[1], shape[0], prim_bound)
    cad_mask = np.where(cad_mask > 0, 1, 0)
    rgb_img = np.array(img)
    rgb_img[:,:,2] = rgb_img[:,:,2] * cad_mask
    return rgb_img

#def mask_gate_connected_road_plan(road_plan, f_lat, f_long):


def mask_existing_roads(shape, f_lat, f_long):
    isACT = check_if_ACT(f_lat, f_long)
    if isACT:
        road_url = ACT_Road_url
    else:
        road_url = NSW_Road_url
    prim_bound, sec_bound, cad_ds = generate_cad_points(f_lat, f_long, isACT)
    #road_plt, points_data = gen_plt_from_url(plt, prim_bound, prim_bound, road_url, 'yellow', '', shape)
    ds = generate_shape(prim_bound, road_url)
    raster_ds = extract_rasterize_points(ds,shape[1],shape[0], prim_bound)
    array = np.array(raster_ds.GetRasterBand(1).ReadAsArray())

    return #pts_to_mask(np.asarray(points_data), shape)

def mask_gates(shape, f_lat, f_long):
    isACT = check_if_ACT(f_lat, f_long)
    if isACT:
        road_url = ACT_Road_url
    else:
        road_url = NSW_Road_url
    ds = generate_entrance(f_lat, f_long, road_url)
    prim_bound, sec_bound, cad_ds = generate_cad_points(f_lat, f_long, isACT)
    points_data = np.asarray(extract_points(ds, shape[0], shape[1], prim_bound))
    for i in range(len(points_data)):
        for j in range(len(points_data)):
            if not i == j:
                if np.linalg.norm(points_data[i,:]-points_data[j,:]) < 20:
                    new_pt = (points_data[j,:] + points_data[i,:]) / 2
                    points_data[j,:] = new_pt
                    points_data[i,:] = new_pt
    proc_points_data = np.unique(points_data, axis=0)

    return pts_to_mask(proc_points_data, shape)

