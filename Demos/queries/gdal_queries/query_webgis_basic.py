import sys, os
sys.path.insert(1, os.getcwd())
from io import BytesIO
from shapely.wkb import loads
from PIL import Image
from Backend.queries.interpolate import generate_grid

from gdal import ogr
from gdal import osr
import gdal
import requests
from pyproj import Proj, transform
import numpy as np

def get_cont_from_point(lat, long, cell_size):
    isACT = check_if_ACT(lat, long)
    prim_bound, other_bound, _ = generate_cad_points(lat, long, isACT)
    if isACT:
        contour_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/contours_2015/MapServer/0/query?outFields=contour&geometry="
    else:
        contour_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/LPIMap/MapServer/69/query?outFields=elevation&geometry="
    return generate_DEM(prim_bound, contour_url, cell_size)

def get_cont_from_two_points(c_1, c_2, cell_size):
    isACT = check_if_ACT(c_1[0], c_1[1])
    bound = generate_rect_bounds(c_1, c_2, isACT)
    if isACT:
        contour_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/contours_2015/MapServer/1/query?outFields=contour&geometry="
    else:
        contour_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/LPIMap/MapServer/69/query?outFields=elevation&geometry="
    return generate_DEM(bound, contour_url, cell_size)

def pts_to_mask(points, shape):
    Binary_Grid = np.zeros(shape)
    for pt in points:
        pt = np.array(np.floor(pt), dtype=np.int)
        if pt.ndim > 1:
            for p in pt:
                if p[1] in range(0, shape[0]) and p[0] in range(0, shape[1]):
                    Binary_Grid[p[1], p[0]] = True
        else:
            if pt[1] in range(0, shape[0]) and pt[0] in range(0, shape[1]):
                Binary_Grid[pt[1], pt[0]] = True
    return Binary_Grid

def generate_entrance(lat, long, Road_url):
    isACT = check_if_ACT(lat, long)
    prim_bound, sec_bound, cad_ds = generate_cad_points(lat, long, isACT)
    road = generate_shape(prim_bound, Road_url)
    print(road)
    road_lyr = road.GetLayer()
    cad_lyr = cad_ds.GetLayer()
    cad_feat = cad_lyr.GetFeature(0)
    cad_geom = loads(cad_feat.GetGeometryRef().ExportToWkb())
    driver = ogr.GetDriverByName( 'Memory' )
    out_ds = driver.CreateDataSource( 'out' )
    out_lyr = out_ds.CreateLayer('', geom_type=ogr.wkbPoint)
    feature_defn = out_lyr.GetLayerDefn()
    for feature in road_lyr:
        geom = loads(feature.GetGeometryRef().ExportToWkb())
        inter_geom = cad_geom.exterior.intersection(geom)
        print(inter_geom)
        if not inter_geom.is_empty:
            if inter_geom.geom_type == 'MultiPoint':
                for p in inter_geom:
                    out_geom = ogr.Feature(feature_defn)
                    out_geom.SetGeometry(ogr.CreateGeometryFromWkb(p.wkb))
                    out_lyr.CreateFeature(out_geom)
                    out_geom.Destroy
            elif inter_geom.geom_type == 'Point':
                out_geom = ogr.Feature(feature_defn)
                out_geom.SetGeometry(ogr.CreateGeometryFromWkb(inter_geom.wkb))
                out_lyr.CreateFeature(out_geom)
                out_geom.Destroy
    return out_ds

def check_if_ACT(lat, long):
    act_border = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/basic/MapServer/0/query?geometry="
    box_bound = generate_box_points(lat, long, 0, True)
    ds = generate_shape(box_bound, act_border)
    lyr = ds.GetLayer()
    return lyr.GetFeatureCount() == 1

def generate_grid_gdal(ds, width=1024, height=1024):
    ds2 = gdal.Grid('', ds, format="MEM", width=width, height=height, zfield="elevation", algorithm="invdist:power=2.0:smoothing=5.0")
    #ds3 = gdal.DEMProcessing('', ds2, 'hillshade', format='MEM', zFactor=3)
    #ds4 = ds2
    #gdal.ReprojectImage(ds2, ds4, '' , '',  gdal.GRA_Cubic)
    #gdal.ReprojectImage(ds2, ds4, '' , '',  gdal.GRA_Cubic)
    array = np.array(ds2.GetRasterBand(1).ReadAsArray())
    array = np.flip(array,0)
    ds2 = None
    return array

def generate_DEM(prim_bound, contour_url, cell_size):
    #cell_size = 1
    contour = generate_shape(prim_bound, contour_url)
    lyr = contour.GetLayer()
    points = np.empty((0,3))
    extent = prim_bound.GetEnvelope()
    for feature in lyr:
        geom = feature.GetGeometryRef()
        type = feature.GetGeometryRef().GetGeometryType()
        if type == 2:
            for pt in geom.GetPoints():
                z = feature.GetField(0)
                x = pt[0]
                y = pt[1]
                if ((x < extent[1]) & (x > extent[0]) & (y < extent[3]) & (y > extent[2])):
                    points = np.append(points, np.array([[x, y, z]],dtype=np.float), axis=0)
        else:
            z = feature.GetField(0)
            x = geom.Centroid().GetX()
            y = geom.Centroid().GetY()
            if ((x < extent[1]) & (x > extent[0]) & (y < extent[3]) & (y > extent[2])):
                points = np.append(points, np.array([[x,y,z]]), axis=0)

    dimensions = [(extent[1] - extent[0]), (extent[3] - extent[2])]
    dimensions = [int(round(e)) for e in dimensions]
    width = int(dimensions[0] / cell_size)
    height = int(dimensions[1] / cell_size)
    #slow scipy process
    dem = generate_grid(points, width, height)
    #fast gdal process
    #dem = generate_grid_gdal(contour, width, height)
    return dem



def generate_rect_bounds(c1, c2, isACT):
    if isACT:
        outputEPSG = 28355
    else:
        outputEPSG = 3857
    inputEPSG = 4326
    in_str = 'epsg:' + str(inputEPSG)
    out_str = 'epsg:' + str(outputEPSG)
    inProj = Proj(in_str)
    outProj = Proj(out_str)
    x,y = transform(inProj,outProj,c1[0], c1[1])
    p1 = [x,y]
    x,y = transform(inProj,outProj,c2[0], c2[1])
    p2 = [x,y]
    point = ogr.Geometry(ogr.wkbLinearRing)
    point.AddPoint(min(p1[0], p2[0]), min(p1[1],p2[1]))
    point.AddPoint(max(p1[0], p2[0]), max(p1[1],p2[1]))
    return point



def generate_box_points(lat, long, range, isACT):
    """
    Generate box coordinates based on the position's lat and long with a side = range * 2

    :param lat: latitude
    :param long: longatude
    :param range: 1/2 side distance of the square box (metres)
    :param isACT: boolean indicating if its within ACT
    :return: box coordinate in the Spatial Reference as defined by the state
    """
    if isACT:
        outputEPSG = 28355
    else:
        outputEPSG = 3857
    inputEPSG = 4326
    in_str = 'epsg:' + str(inputEPSG)
    out_str = 'epsg:' + str(outputEPSG)
    inProj = Proj(in_str)
    outProj = Proj(out_str)
    x, y = transform(inProj, outProj, lat, long)
    bound= ogr.Geometry(ogr.wkbLinearRing)
    bound.AddPoint(x-range, y-range)
    bound.AddPoint(x+range, y+range)
    return bound

def generate_bound(min_x, min_y, max_x, max_y):
    bound= ogr.Geometry(ogr.wkbLinearRing)
    bound.AddPoint(min_x, min_y)
    bound.AddPoint(max_x, max_y)
    return bound


def generate_image_URL(bound, feature_url, q_x, q_y):

    """
    Generate the full image query URL

    :param bound: the box coordinates
    :param feature_url: the feature_url of the map/image server
    :return: full url for query image
    """

    url = feature_url + str(bound.GetPoint(0)[0])+ "%2C" + str(bound.GetPoint(0)[1]) + "%2C" + str(bound.GetPoint(1)[0]) + "%2C" + str(bound.GetPoint(1)[1])
    url = url + "&size=" + str(q_x) + "%2C" + str(q_y)
    url = url + "&format=png&transparent=true&f=image"
    return url


def generate_shape_URL(bound, feature_URL):
    """
    Generate the full shape query URL

    :param bound: box coordinate
    :param feature_URL: the feature_url of the map server
    :return: full url for query shape
    """
    url = feature_URL
    url = url + str(bound.GetPoint(0)[0])+ "," + str(bound.GetPoint(0)[1]) + "," + str(bound.GetPoint(1)[0]) + "," + str(bound.GetPoint(1)[1])
    url = url + "&geometryType=esriGeometryEnvelope&f=pjson"
    return url


def generate_shape_file(bound, feature_URL, filename):
    """
    Generate a shapefile from the query return

    :param bound: box coordinate
    :param feature_URL: the feature_url of the map serevr
    :param filename: filename to save to
    :return: null
    """
    url = generate_shape_URL(bound, feature_URL)
    print(url)
    ds = ogr.Open(url)
    lyr = ds.GetLayer()
    outShp = filename + ".shp"
    outDriver = ogr.GetDriverByName("ESRI Shapefile")
    outDataSource = outDriver.CreateDataSource(outShp)
    outDataSource.CopyLayer(lyr,"test")
    return



def generate_shape(bound, feature_url):
    """
    Generate a ogr dataset from the query return

    :param bound: box coordinate
    :param feature_url: the feature_url of the map serevr
    :return: response dataset of the query
    """
    url = generate_shape_URL(bound, feature_url)
    print(url)
    response = requests.get(url, headers={'Cache-Control': 'no-cache'})
    data = response.content
    ds = gdal.OpenEx(data)
    return ds

def generate_image(bound, feature_url, q_x, q_y):
    """
    Generate an Image

    :param bound:
    :param feature_url:
    :return:
    """
    url = generate_image_URL(bound, feature_url, q_x, q_y)
    ret = requests.get(url)
    print(url)
    img = Image.open(BytesIO(ret.content))
    return img

def gen_plt_from_url(plt, data_bound, map_bound, url, color, pt_shp, size):
    ds = generate_shape(data_bound, url)
    points_data = extract_points(ds, size[0], size[1], map_bound)
    for points in points_data:
        if points.ndim > 1:
            plt.plot(points[:, 0], points[:, 1], color = color, rasterized=True)
        else:
            plt.plot(points[0], points[1], pt_shp,  color = color, markersize=15, rasterized=True)
    return plt, points_data

def gen_plt_from_points(points_data, plt, color, pt_shp):
    for points in points_data:
        if points.ndim > 1:
            plt.plot(points[:, 0], points[:, 1], color = color)
        else:
            plt.plot(points[0], points[1], pt_shp,  color = color, markersize=15)
    return plt

def extract_rasterize_points(ds, img_x, img_y, bound):
    points_data = []
    lyr = ds.GetLayer()
    points = np.empty((0, 2))
    min_X = bound.GetPoint(0)[0]
    max_X = bound.GetPoint(1)[0]
    min_Y = bound.GetPoint(0)[1]
    max_Y = bound.GetPoint(1)[1]
    height = int((max_X - min_X) / img_x)
    width = int((max_Y - min_Y) / img_y) 
    if os.path.exists('temp.tiff'):
        os.remove('temp.tiff')
    out_driver = gdal.GetDriverByName('GTiff')
    out_ds = out_driver.Create('test.tiff', int(img_x), int(img_y), 1, gdal.GDT_Float32)
    out_ds.SetGeoTransform((min_X, width, 0, max_Y, 0, -height))
    out_ds.SetProjection(lyr.GetSpatialRef().ExportToWkt())
    out_lyr = out_ds.GetRasterBand(1)
    out_lyr.SetNoDataValue(0)
    gdal.RasterizeLayer(out_ds, [1], lyr)
    arr = np.array(out_ds.GetRasterBand(1).ReadAsArray())
    if os.path.exists('temp.tiff'):
        os.remove('temp.tiff')
    return arr

#def convert_polygon_to_polylines(ds):
#    lyr = ds.GetLayer()
#    for feat in lyr:




def extract_points(ds, img_x, img_y, bound):
    points_data = []
    lyr = ds.GetLayer()
    points = np.empty((0, 2))
    min_X = bound.GetPoint(0)[0]
    max_X = bound.GetPoint(1)[0]
    min_Y = bound.GetPoint(0)[1]
    max_Y = bound.GetPoint(1)[1]
    dim = [(max_X - min_X), (max_Y - min_Y)]
    #print(dim[0], dim[1])
    ratio_x = img_x / dim[0]
    ratio_y = img_y / dim[1]
    for feature in lyr:
        type = feature.GetGeometryRef().GetGeometryType()
        if  type == ogr.wkbPolygon:
            for geo in feature.GetGeometryRef():
                x = np.array([geo.GetX(i) for i in range(geo.GetPointCount())])
                y = np.array([geo.GetY(i) for i in range(geo.GetPointCount())])
                points = np.array([x,y]).transpose()
            points[:,0] = ratio_x * (points[: , 0] - min_X)
            points[:,1] = img_y - (ratio_y * (points[: , 1] - min_Y))

        if type == ogr.wkbLineString:
            for pt in feature.GetGeometryRef().GetPoints():
                points = np.concatenate((points, [np.array(pt)]))
            points[:,0] = ratio_x * (points[: , 0] - min_X)
            points[:,1] = img_y - (ratio_y * (points[: , 1] - min_Y))
        if type == ogr.wkbPoint:
            points = np.array([feature.GetGeometryRef().GetX(), feature.GetGeometryRef().GetY()])
            points[0] = ratio_x * (points[0] - min_X)
            points[1] = img_y - (ratio_y * (points[1] - min_Y))

        points_data.append(points)
        points = np.empty((0, 2))
    return points_data

def generate_cad_points(lat, long, isACT):
    """
    Generates coordinate box in both NSW (3857) and ACT (28355) Spatial Reference

    :param lat: latitude
    :param long: longatude
    :param isACT: boolean indicating if its within ACT
    :return: bound - coordinate in spatial reference of its State (if isACt = true, ACT spatial reference), other - cooridate in spatial reference of the other state
    """
    if isACT:
        outputEPSG = 28355
        Cadastre_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/basic/MapServer/101/query?geometry="
    else:
        Cadastre_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/Cadastre/MapServer/0/query?geometry="
        outputEPSG = 3857

    box_bound = generate_box_points(lat, long, 0, isACT)
    ds = generate_shape(box_bound, Cadastre_url)
    lyr = ds.GetLayer()
    extent = lyr.GetExtent()
    bound = ogr.Geometry(ogr.wkbLinearRing)
    bound.AddPoint(extent[0], extent[2])
    bound.AddPoint(extent[1], extent[3])

    if isACT:
        inEPSG = 28355
        outEPSG = 3857
    else:
        outEPSG = 28355
        inEPSG = 3857

    in_str = 'epsg:' + str(inEPSG)
    out_str = 'epsg:' + str(outEPSG)
    inProj = Proj(in_str)
    outProj = Proj(out_str)
    other = ogr.Geometry(ogr.wkbLinearRing)
    x, y = transform(inProj, outProj, extent[0], extent[2])
    other.AddPoint(x,y)
    x, y = transform(inProj, outProj, extent[1], extent[3])
    other.AddPoint(x, y)

    #inSpatialRef = osr.SpatialReference()
    #inSpatialRef.ImportFromEPSG(inEPSG)
    #outSpatialRef = osr.SpatialReference()
    #outSpatialRef.ImportFromEPSG(outEPSG)
    #coordTransform = osr.CoordinateTransformation(inSpatialRef, outSpatialRef)
    #other = bound.Clone()
    #other.Transform(coordTransform)
    return bound, other, ds

