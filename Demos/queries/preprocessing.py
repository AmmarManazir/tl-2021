import numpy as np
import pickle
import time
import psycopg2
from Backend.queries.interpolate import generate_grid


# setup the database before running this script

start = time.time()
too_many_points = []
not_enough_points = []

def connect_to_database(user, password, database, host="localhost", port="5432"):
    connection = psycopg2.connect(user=user,
                                  password=password,
                                  database=database,
                                  host=host,
                                  port=port)
    return connection


def preprocess_all_dems(connection, ids, cell_size=1):
    """
        Creates a database table with id, address, boundary, dem for each property

        :param connection:
        :param ids: List of property ids
        :param cell_size: The size of each cell in meters. e.g. cell_size = 10 means each cell is 10mx10m
    """
    total = len(ids)
    max_cells = 1e6
    max_cells_sqrt = int(np.sqrt(max_cells))
    for i, id_ in enumerate(ids):
        boundary = query_boundary_geom(connection, id_)
        extent = query_extent(connection, id_)
        address = query_address(connection, id_)
        dimensions = [(extent[2] - extent[0]), (extent[3] - extent[1])]
        dimensions = [int(round(e)) for e in dimensions]
        width = int(dimensions[0] / cell_size)
        height = int(dimensions[1] / cell_size)

        if width*height > max_cells:
            too_many_points.append(str(id_))
            continue

        uninterpolated = query_uninterpolated_points(connection, id_)

        # Skip properties with less than 4 points
        if len(uninterpolated) < 4:
            interpolated = np.empty(0)
            not_enough_points.append(str(id_))
        else:
            interpolated = generate_grid(uninterpolated, width, height)

        insert_dem(connection, id_, address, boundary, interpolated)
        print(f"Properties processed: {i+1}/{total}, "
              f"Hours running: {round((time.time()-start)/3600, 6)}", end='\r')


def create_empty_dem_table(connection):
    connection.set_session(autocommit=True)
    cursor = connection.cursor()
    cursor.execute(
        """
        Drop table if exists dems;
        CREATE TABLE dems (
            id numeric,
            address character varying,
            boundary geometry,
            dem BYTEA
        );
        """
    )


def all_property_ids(connection):
    cursor = connection.cursor()
    query = """select id from act_blocks;"""
    cursor.execute(query)
    ids = cursor.fetchall()
    ids = [id_[0] for id_ in ids]
    return ids


def query_boundary_geom(connection, id_):
    cursor = connection.cursor()
    cursor.execute(f"""select wkb_geometry from act_blocks where id={id_}""")
    geom = cursor.fetchone()
    return geom[0]


def query_extent(connection, id_):
    cursor = connection.cursor()
    cursor.execute(f"""select St_Extent(wkb_geometry) from act_blocks where id={id_};""")
    extent = cursor.fetchone()

    # Converts ('BOX(minX, minY, maxX, maxY)') to [minX, minY, maxX, maxY]
    e = extent[0]
    e = e[e.find('(') + 1:e.find(')')].replace(",", " ").split()
    e = [float(s) for s in e]
    return e


def query_address(connection, id_):
    cursor = connection.cursor()
    query = f"""select addresses from act_blocks where id={id_};"""
    cursor.execute(query)
    address = cursor.fetchone()
    return address


def query_uninterpolated_points(connection, id_):
    # 1. Select the contours that intersect (or are inside) the property boundary
    # 2. Convert these contours to points
    # 3. Select just the points inside the property boundary
    cursor = connection.cursor()
    cursor.execute(f"""SELECT ST_X((dp).geom) AS x, ST_Y((dp).geom) AS y, z FROM 

                    (SELECT ST_DumpPoints(act_contours.wkb_geometry) AS dp, contour AS z 
                    FROM act_contours, 

                    (select ST_SetSRID(ST_Extent(wkb_geometry),28355) as wkb_geometry from act_blocks where id={id_}) as boundary

                    WHERE ST_intersects(boundary.wkb_geometry, act_contours.wkb_geometry)
                    ) AS points, 

                    (select ST_SetSRID(ST_Extent(wkb_geometry),28355) as wkb_geometry from act_blocks where id={id_}) as boundary

                    WHERE st_contains(boundary.wkb_geometry, (dp).geom);""")
    points = np.array(cursor.fetchall())
    return points


def insert_dem(connection, id_, address, boundary, dem):
    cursor = connection.cursor()
    cursor.execute(
        f"""
        INSERT INTO dems(id, address, boundary, dem)
        VALUES (%s, %s, %s, %s)
        """,
    (id_, address, boundary, pickle.dumps(dem))
    )


if __name__ == '__main__':
    connection = connect_to_database(user="me", password="me", database="act", host="localhost", port="5432")
    # create_empty_dem_table(connection)
    # ids = all_property_ids(connection)
    # start = time.time()
    # # preprocess_all_dems(connection, [195058], cell_size=10)     # Just process spring valley (5 secs)
    # preprocess_all_dems(connection, ids, cell_size=10)      # Process all of the ACT (5 hours)
    #
    # print("\nProcessing complete")
    # print(f"Processing took {time.time() - start} seconds")
    # print(f"Number of properties with too many points  : {len(too_many_points)}")
    # print(f"Number of properties with not enough points: {len(not_enough_points)}")
    #
    # print(f"Properties with too many points  : {too_many_points}")
    # print(f"Properties with not enough points: {not_enough_points}")

    connection.close()






