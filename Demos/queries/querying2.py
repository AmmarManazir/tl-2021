import psycopg2
import numpy as np
import matplotlib.pyplot as plt


def connect_to_database(user, password, database, host="localhost", port="5432"):
    connection = psycopg2.connect(user=user,
                                  password=password,
                                  database=database,
                                  host=host,
                                  port=port)
    return connection


def get_points(cursor, corners, table="uninterpolated", corners_epsg=28355, table_epsg=28355, sorted=False):
    """Fetch all the points in a given area"""
    # I think I should have a 'get_points_interpolated' and 'get_points_uninterpolated' function.
    # Transform the points, not the polygon.
    polygon = f"""select ST_SetSRID(ST_Extent(ST_MakeLine(ARRAY[p1, p2, p3, p4, p1])), {table_epsg}) as polygon
                from
                (select ST_Transform(ST_SetSRID(ST_Point({corners["min_x"]}, {corners["min_y"]}), {corners_epsg}), {table_epsg}) as p1) as foo,
                (select ST_Transform(ST_SetSRID(ST_Point({corners["max_x"]}, {corners["min_y"]}), {corners_epsg}), {table_epsg}) as p2) as bar,
                (select ST_Transform(ST_SetSRID(ST_Point({corners["max_x"]}, {corners["max_y"]}), {corners_epsg}), {table_epsg}) as p3) as roar,
                (select ST_Transform(ST_SetSRID(ST_Point({corners["min_x"]}, {corners["max_y"]}), {corners_epsg}), {table_epsg}) as p4) as nein
    """
    query = f"select st_x(point) as x, st_y(point) as y, z from {table}, ({polygon}) as bla where st_contains(polygon, point)"
    if sorted:
        query = f"{query} ORDER BY x, y ASC"
    cursor.execute(query)
    fetched = cursor.fetchall()
    if not fetched:
        print(f"No points found for corners {corners}")
    points = np.asarray(fetched)
    return points

def tablify_points(points, cell_size=10):
    """Convert points from [(x,y,z), ... ] to a 2d array of z values. Assumes points are pre-interpolated."""
    shape = (len(set(points[:,0])), len(set(points[:,1])))
    dem = points[:,2].reshape(shape)
    return dem

if __name__ == '__main__':
    connection = connect_to_database("me", "me", "nsw")
    cursor = connection.cursor()
    cell_size = 10

    # Big area
    point1 = "-36.135280, 148.858871"
    point2 = "-36.262935, 148.715140"

    # smaller area
    point1 = "-36.324460, 148.711146"
    point2 = "-36.282180, 148.781133"

    point1 = point1.split(", ")
    point2 = point2.split(", ")
    corners = {
        "min_x": point1[1],
        "min_y": point1[0],
        "max_x": point2[1],
        "max_y": point2[0],
    }
    points = get_points(cursor, corners, table="berridale", corners_epsg=4326, table_epsg=28355)
    dem = tablify_points(points, corners)
    # plt.scatter(points[:, 0], points[:, 1], s=1, c=points[:,2])
    # plt.show()

    print()
