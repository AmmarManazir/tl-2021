import time

class Progress_estimator():
    """Print estimates for how long the loop will take"""
    def __init__(self, total_iterations, loop_name=""):
        self.current_iteration = 0
        self.total_iterations = total_iterations
        self.start_time = time.time()
        self.expected_completion = self.start_time
        self.loop_name = loop_name
        print(f"{self.loop_name} starting loop")

    def __call__(self):
        if self.current_iteration > 0 and time.time() > self.expected_completion:
            iteration_time = (time.time() - self.start_time)/self.current_iteration
            self.expected_completion = time.time() + (total_iterations * iteration_time)
            print(f"{self.loop_name} {self.current_iteration}/{self.total_iterations}, expected completion time: {time.ctime(self.expected_completion)}")
        self.current_iteration += 1


if __name__ == '__main__':
    total_iterations = 100
    progress_estimator = Progress_estimator(total_iterations, "test")
    for i in range(total_iterations):
        progress_estimator()
        time.sleep(0.1)
        if i > 2:
            time.sleep(0.2)