import numpy as np
from Backend.queries.interpolate import generate_grid
import matplotlib.pyplot as plt
from Demos.util.file_management import get_root, load_dem
from Demos.util.debugging import Progress_estimator
import time
from Demos.queries.querying2 import get_points, connect_to_database

def get_boundary(cursor):
    query = "select min(st_x(point)), min(st_y(point)), max(st_x(point)), max(st_y(point)) from uninterpolated"
    cursor.execute(query)
    fetched = cursor.fetchall()[0]
    full_boundary = {"min_x":fetched[0], "min_y":fetched[1], "max_x":fetched[2], "max_y":fetched[3]}
    return full_boundary

def attach_xy(grid, corners, cell_size, boundary):
    """Attach x and y values to each z value in the grid
        params:
            grid = 2d numpy array of ints
            corners = {min_x: _, min_y: _ , max_x: _, max_y: _}
        return:
            list of [x,y,z] based off the grid"""
    if len(grid) == 0:
        return []
    interpolated = []
    for i in range(len(grid)):
        for j in range(len(grid[0])):
            # Important to note that I'm recording the coordinate in the center of the cell. (not bottom left)
            x = corners["min_x"] + cell_size * i + cell_size/2
            y = corners["min_y"] + cell_size * j + cell_size/2
            z = int(round(grid[i][j]))
            # Don't include points that are outside the uninterpolated table
            if x > boundary["min_x"] and x < boundary["max_x"] and y > boundary["min_y"] and y < boundary["max_y"]:
                interpolated.append((x,y,z))
    return interpolated

def insert_points(connection, cursor, points, table="interpolated", epsg=28355):
    """Insert the points into the table"""
    # start = time.time()
    values = ', '.join(f"(ST_SetSRID(ST_POINT({p[0]}, {p[1]}), {epsg}), {p[2]})" for p in points)
    query = f"INSERT INTO {table} VALUES {values}"
    cursor.execute(query)
    connection.commit()
    # print(f"Time taken to insert {len(points)} points: {time.time() - start}")

if __name__ == '__main__':
    connection = connect_to_database("me", "me", "nsw")
    cursor = connection.cursor()
    # Add in a drop table interpolated so I don't have to do this manually
    boundary = get_boundary(cursor)
    cell_size = 10
    buffer_size = 100
    batch_size = 1000
    width = height = batch_size//cell_size
    num_batches_x = (int(boundary["max_x"]-boundary["min_x"])//batch_size) + 1
    num_batches_y = (int(boundary["max_y"]-boundary["min_y"])//batch_size) + 1

    # num_batches_x = 2
    # num_batches_y = 2
    all_interpolated = []

    progress_estimator = Progress_estimator(num_batches_x * num_batches_y, "Interpolation loop")
    for i in range(num_batches_x):
        for j in range(num_batches_y):
            corners = {"min_x":boundary["min_x"] + batch_size*i,
                     "min_y":boundary["min_y"] + batch_size*j,
                     "max_x":boundary["min_x"] + batch_size*i + batch_size,
                     "max_y":boundary["min_y"] + batch_size*j + batch_size}
            corners_with_buffer = {"min_x":corners["min_x"] - buffer_size,
                                 "min_y":corners["min_y"] - buffer_size,
                                 "max_x":corners["max_x"] + buffer_size,
                                 "max_y":corners["max_y"] + buffer_size}
            points = get_points(cursor, corners_with_buffer)
            grid = generate_grid(points, width, height, corners)
            if len(grid) > 0:
                grid = np.rot90(grid, k=3)   # Not sure about this yet
                interpolated = attach_xy(grid, corners, cell_size, boundary)
                # all_interpolated.extend(interpolated)
                insert_points(connection, cursor, interpolated)
            progress_estimator()

    # all_interpolated = np.asarray(all_interpolated)
    # print(len(all_interpolated))
    # plt.scatter(all_interpolated[:, 0], all_interpolated[:, 1])
    # print()
    # Generate a matplotlib plot with x against y