## Using QGIS
The postgres queries can all be done using QGIS if needed.

### Basic usage
- To add a base layer, expand the `XYZ Tiles` in the browser & double click on `OpenStreetMap`  
- To add a google map base layer, right click on `XYZ Tiles` and make a `New Connection` using a url from [here](https://www.hatarilabs.com/ih-en/how-to-add-a-google-map-in-qgis-3-tutorial). I recommend terrain view.
- To add your own layers, find them in the browser and double click on them.
- To see a layer, right click on it and select `Zoom to Layer`
    
### Export layer
- right click on the layer, Export -> Save As…, choose the format you want. e.g. postgreSQL SQL dump

### Extracting x,y,z coordinates for a given area
- To Clip: At the top of the window click Vector -> Geoprocessing Tools -> Clip…, then choose the contour layer & polygon mask layer
- To Convert to XYZ: choose Vector -> Geometry Tools -> Extract Vertices…, you can then export that as a .csv file.

### Convert from Raster to Vector
- Raster -> Conversion -> Polygonize (Raster to Vector)
- To visualise multiple vector layers at once, double click on the layer, symbology -> outline green (can then change the colour to whatever you want)
- Can also just change the opacities