import os
import numpy as np

def get_root():
    cwd = os.getcwd()
    repo_name = 'civilise.ai-techlauncher-2020'
    root_path = cwd[:cwd.find(repo_name) + len(repo_name)]
    return root_path

def load_dem(name):
    return np.loadtxt(os.path.join(get_root(),"Demos","Data", name))