import numpy as np
from scipy.interpolate import griddata
from skimage.feature import peak_local_max
import matplotlib.pyplot as plt


def generateGrid(points, width=1024, height=2048):
    """
        author: Yasar
         
    """
    z = points[:, 2]
    # Define the grid
    start_point = np.min(points, axis=0)
    end_point = np.max(points, axis=0)

    # Use meshgrid to generate a grid map, flip y because the latitude
    X, Y = np.meshgrid(np.linspace(start_point[0], end_point[0], width+2),
                       np.linspace(end_point[1], start_point[1], height+2))

    # Use interpolate to interpolate unstructured D-D data
    Z = griddata(points[:, 0:2], z, (X, Y), method='cubic')

    return X[1:-1, 1:-1], Y[1:-1, 1:-1], Z[1:-1, 1:-1]


def find_local_peaks(DEM, windowSize, display = False):
    """
    windowSize represents the size of window in which you want to find the maximum
    """
    DEM_nanless = np.nan_to_num(DEM)
    coords_peak = peak_local_max(DEM_nanless, min_distance=windowSize)
    
    
    if display:
        fig, ax = plt.subplots() 
        ax.imshow(DEM_nanless, cmap = plt.cm.gray)
        ax.autoscale(False) 
        ax.plot(coords_peak[:, 1], coords_peak[:, 0], 'r.')
        ax.axis('off')
        ax.set_title('Peak local max')

        fig.subplots_adjust(wspace=0.02, hspace=0.02, top=0.9,
                            bottom=0.02, left=0.02, right=0.98)
        plt.show()
    
    return coords_peak
