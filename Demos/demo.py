import sys, os
sys.path.insert(1, os.getcwd())

import numpy as np
import matplotlib.pyplot as plt
import plotly.graph_objects as go

from Backend.trench_plan.evenly_spaced_trenches import trench_plan as trench_plan_evenly_spaced
from Backend.ridge_and_gully.pysheds_accumulation import gullies, ridges, water_accumulation, closure
from Demos.trench_plan.demo_step_terrain import visualise_creek_2d, plot_cross_section
from Backend.trench_plan.step_terrain import trench_plan as trench_plan_step_terrain


if __name__ == '__main__':
    property = "spring_valley"
    # properties = "spring_valley", "black_mountain", "rob_roy", "swamp_creek"
    properties = ["rob_roy"]
    for property in properties:
        outpath = f'../Images/{property}'
        pycharm_path = f'Data/{property}.txt'
        path_from_root = f'Backend/Data/{property}.txt'
        Z = np.loadtxt(pycharm_path if os.path.exists(pycharm_path) else path_from_root)

        # Sliders
        num_trenches_evenly_spaced = 5    # Ideal range: 1-10 ish. May want a higher value for finding paddocks, but lower for actual trenches.
        num_trenches_step_terrain = 3
        acc_threshold = 99.5  # Ideal range: 98-99.5 ish (it's a percentile)

        # Step terrain visualisation
        creek_plot = visualise_creek_2d(Z, num_trenches=num_trenches_step_terrain, title=f"{property} main creek and chosen contours")
        # creek_plot.show()
        creek_plot.savefig(f'{outpath}3_creek_top_down.png')
        plt.close()

        cross_section = plot_cross_section(Z, title=f"{property} cross section of main creek")
        # cross_section.show()
        cross_section.savefig(f'{outpath}4_creek_cross_section.png')
        plt.close()

        # The main functions
        t = trench_plan_evenly_spaced(Z, num_trenches=num_trenches_evenly_spaced)     # Expected changes: Jinzhi's step terrrain approach
        g = gullies(Z, acc_threshold)
        r = ridges(Z, acc_threshold)    # Expected changes: Roger's better gullies & ridges approach, and/or Yasar's better connectivity approach

        # Closure
        acc_r = water_accumulation(-Z)
        acc_g = water_accumulation(Z)
        closure(acc_r, acc_g, r, g)

        # 2D matplotlib visualisation - evenly spaced trenches
        plt.imshow(Z)
        plt.title(f"{property} with {num_trenches_evenly_spaced} trenches, ridge/gully threshold: {acc_threshold}")
        y, x = np.where(t == 1)
        plt.scatter(x, y, marker='.', linewidths=.1, c='green')
        y, x = np.where(g == 1)
        plt.scatter(x, y, marker='.', linewidths=.1, c='blue')
        y, x = np.where(r == 1)
        plt.scatter(x, y, marker='.', linewidths=.1, c='red')
        # plt.show()
        plt.savefig(f'{outpath}2_all_2d.png')
        plt.close()
        #
        #
        # # 3D plotly visualisation - evenly spaced trenches
        Z = Z[::-1]
        t = trench_plan_evenly_spaced(Z, num_trenches=num_trenches_evenly_spaced)     # Expected changes: Jinzhi's step terrrain approach
        g = gullies(Z, acc_threshold)
        r = ridges(Z, acc_threshold)    # Expected changes: Roger's better gullies & ridges approach, and/or Yasar's better connectivity approach
        t2 = trench_plan_step_terrain(Z, num_trenches=num_trenches_step_terrain)     # Expected changes: Jinzhi's step terrrain approach

        # Closure
        acc_r = water_accumulation(-Z)
        acc_g = water_accumulation(Z)
        closure(acc_r, acc_g, r, g)

        fig = go.Figure(data=[go.Surface(z=Z, colorscale='Viridis', showscale=True)])
        fig.update_traces(contours_z=dict(show=True, usecolormap=True,
                                          highlightcolor="limegreen", project_z=True))
        fig.add_scatter3d(x=np.where(t == True)[1],
                          y=np.where(t == True)[0],
                          z=Z[t == True] + 3,
                          mode='markers',
                          marker=dict(
                              size=1,
                              color='green',
                              opacity=.8
                          ))
        fig.add_scatter3d(x=np.where(g == True)[1],
                          y=np.where(g == True)[0],
                          z=Z[g == True] + 3,
                          mode='markers',
                          marker=dict(
                              size=1,
                              color='blue',
                              opacity=.8
                          ))
        fig.add_scatter3d(x=np.where(r == True)[1],
                          y=np.where(r == True)[0],
                          z=Z[r == True] + 3,
                          mode='markers',
                          marker=dict(
                              size=1,
                              color='red',
                              opacity=.8
                          ))
        # fig.add_scatter3d(x=np.where(t2 == True)[1],
        #                   y=np.where(t2 == True)[0],
        #                   z=Z[t2 == True] + 3,
        #                   mode='markers',
        #                   marker=dict(
        #                       size=1,
        #                       color='orange',
        #                       opacity=.8
        #                   ))
        fig.update_layout(
            title=f"{property} with {num_trenches_evenly_spaced} trenches, ridge/gully threshold: {acc_threshold}",
            autosize=True,
            margin=dict(l=65, r=50, b=65, t=90))
        # fig.show()
        fig.write_image(f'{outpath}1_all_3d.png')


