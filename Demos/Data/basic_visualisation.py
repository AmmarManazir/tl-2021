import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':

    examples = {
    "spring_valley": "-35.28878, 149.01080",
    "black_mountain" : "-35.270872, 149.099910",
    "swamp_creek" : "-35.2629, 148.9205",
    "mulligans_flat": "-35.1669, 149.1609",
    "rob_roy": "-35.5116, 149.1040",
    "caloola_farm": "-35.6716, 149.0727",
    }

    Z = np.loadtxt('caloola_farm.txt')
    plt.imshow(Z)
    plt.show()