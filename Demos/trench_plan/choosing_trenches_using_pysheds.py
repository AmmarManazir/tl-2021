import sys, os
sys.path.insert(1, os.getcwd())
from Archive.Sem1.backend.Scripts.Water_Model import dem_to_grid
import numpy as np
import plotly.graph_objects as go
import matplotlib.pyplot as plt
from Backend.trench_plan.Extract_Contours import extract_contours
from Backend.trench_plan import Extract_Trench

"""
Plot trenches right before water starting to speed up
:param Z: the elevation values of a mxn cells
:param num_trenches: number of trenches to plot
"""


def find_trenches(Z, num_trenches, n):
    grid = dem_to_grid(Z)

    # water accumulation
    grid.fill_depressions(data='dem', out_name='flooded_dem')
    grid.resolve_flats(data='flooded_dem', out_name='inflated_dem')
    grid.flowdir(data='inflated_dem', out_name='dir')
    grid.accumulation(data='dir', out_name='acc')

    dem = np.array(np.round(Z), dtype=int)
    acc = np.array(grid.view('acc'))

    max_accumulation = np.where(acc == np.max(acc))

    # find the creek
    y_axis = max_accumulation[0][0]
    x_axis = max_accumulation[1][0]
    exit_points = [(y_axis, x_axis)]
    # exit_points = find_exit_points(acc)
    """coordinates_array = []

    for coordinate in exit_points:
        while coordinate != (None, None):
            coordinates_array.append(coordinate)
            coordinate = find_nearest_cells_with_highest_flow_rate(coordinate, acc, n)"""

    coordinates_array = find_coordinates_array(Z, n)

    # find the elevation of the trenches
    elvt_diff_ratio_array = elevation_diff_ratio(coordinates_array, acc)
    section_length = int(round(len(elvt_diff_ratio_array)/num_trenches))
    split_elvt_diff_ratio_array = [elvt_diff_ratio_array[section_length * i:]
                                    if ((len(elvt_diff_ratio_array) - section_length*(i+1)) < num_trenches)
                                    else elvt_diff_ratio_array[section_length * i: section_length * (i + 1)]
                                    for i in range(int(num_trenches))]
    coord_index_array = [(diff_ratio_index(split_elvt_diff_ratio_array[i]) + section_length*i)
                         for i in range(int(num_trenches))]

    trench_cells = [coordinates_array[index+1] for index in coord_index_array]

    # plot the trenches and the creek
    creek_coordinates = np.zeros(dem.shape, dtype=float)
    creek_slope = [float(0)] + elvt_diff_ratio_array + [float(0)]
    for coord in coordinates_array:
        creek_coordinates[coord[0], coord[1]] = creek_slope[coordinates_array.index(coord)]
    return creek_slope, coordinates_array, trench_cells

def find_coordinates_array(Z, n):
    grid = dem_to_grid(Z)

    # water accumulation
    grid.fill_depressions(data='dem', out_name='flooded_dem')
    grid.resolve_flats(data='flooded_dem', out_name='inflated_dem')
    grid.flowdir(data='inflated_dem', out_name='dir')
    grid.accumulation(data='dir', out_name='acc')

    dem = np.array(np.round(Z), dtype=int)
    acc = np.array(grid.view('acc'))

    max_accumulation = np.where(acc == np.max(acc))

    # find the creek
    y_axis = max_accumulation[0][0]
    x_axis = max_accumulation[1][0]
    exit_points = [(y_axis, x_axis)]
    # exit_points = find_exit_points(acc)
    coordinates_array = []

    for coordinate in exit_points:
        while coordinate != (None, None):
            coordinates_array.append(coordinate)
            coordinate = find_nearest_cells_with_highest_flow_rate(coordinate, acc, n)
    return coordinates_array

"""
Find all the exit points of the property with accumulation value lager than the adjacent cells. 
:param acc: a 2D grid of all accumulation values of all the cells
"""
def find_exit_points(acc):
    exit_point_coord = []
    for y in (0, len(acc)-1):
        for x in range(len(acc[y])):
            if (x-1) >= 0 and (x+1) <len(acc[y]):
                if acc[y, x] >= acc[y, x-1] and acc[y, x] >= acc[y, x+1]:
                    exit_point_coord.append((y, x))
    """exit_point_coord = [[(y, x) for x in range(len(acc[y]))
                        if (x-1) >= 0 and (x+1) < len(acc[y])
                        if acc[y, x] >= acc[y, x-1] and acc[y, x] >= acc[y, x+1]]
                        for y in (0, len(acc)-1)]"""
    for x in (0, len(acc[0])-1):
        for y in range(len(acc)):
            if (y-1) >= 0 and (y+1) < len(acc):
                if acc[y, x] >= acc[y-1, x] and acc[y, x] >= acc[y+1, x]:
                    exit_point_coord.append((y, x))
    return exit_point_coord

"""
Find the creek leading to the cell with the highest flow rate
:param coord: the coordinate of the cell with the highest flow rate
:param acc: a 2D array of accumulation values for all coordinates
"""
def find_nearest_cells_with_highest_flow_rate(coord, acc, n):
    y_axis = coord[0]
    x_axis = coord[1]
    x = None
    y = None
    neighbours = n_steps_away(y_axis, x_axis, n)
    max_acc = acc[y_axis, x_axis]
    accmulation = 0
    for point in neighbours:
        if point[0] >=0 and point[1] >=0 and point[0] < len(acc) and point[1] < len(acc[0]):
            if accmulation <= acc[point[0], point[1]] and acc[point[0], point[1]]<max_acc:
                accmulation = acc[point[0], point[1]]
                y = point[0]
                x = point[1]
    coordinates = (y, x)

    return coordinates

"""
Find cells n steps away from the chosen cell
:param y: the y axis of the chosen cell
:param x: the x axis of the chosen cell
:param n: the maximum distance from the chosen cell
"""
def n_steps_away(y, x, n):
    neighbours = []
    for i in range(-int(n), int(n)+1):
        for j in range(-int(n), int(n)+1):
            neighbours.append((y+i, x+j))
    neighbours.remove((y,x))
    return neighbours

"""
Calculate the elevation difference between adjacent heights in these cells 
and then the ratio between each cell and the one below
:param coord_array: the coordinates of cells in the creek 
:param dem: a 2D array of elevation values for all cells
"""
def elevation_diff_ratio(coord_array, dem):
    elevation_array = [dem[coord[0], coord[1]] for coord in coord_array]

    elvt_diff_array = [(elevation_array[i+1] - elevation_array[i]) for i in range(len(elevation_array)-1)]

    elvt_diff_ratio_array = [1 if (elvt_diff_array[j+1] == 0) else (elvt_diff_array[j]/elvt_diff_array[j+1])
                             for j in range(len(elvt_diff_array)-1)]
    return elvt_diff_ratio_array

"""
Find the index of the smallest ratio in the elevation diff ratio array
:param ratio_array: the elevation diff ratio array
"""
def diff_ratio_index(ratio_array):
    smallest_ratios = min(ratio_array)
    ratio_index = ratio_array.index(smallest_ratios)
    return ratio_index

def plot_result(Z, r, title, check_num, color):
    y, x = np.where(r == check_num)
    plt.imshow(Z)
    plt.scatter(x, y, marker='.', linewidths=.1, c=color)
    plt.title(title)
    return plt

def plot_creek(Z, r, title, check_array):
    y = [coord[0] for coord in r]
    x = [coord[1] for coord in r]
    plt.imshow(Z)
    plt.scatter(x, y, marker='.', linewidths=.1, c=check_array, cmap='gray_r')
    plt.title(title)
    return plt

def display_trench(trench, Z):
    if type(trench) != None:
        trench_x, trench_y = trench[:,0].astype(np.uint64), trench[:,1].astype(np.uint64)
    Z[trench_x, trench_y] = 255
    plt.imshow(Z)
    return plt

def visualise_creek(Z, r, fig):
    y = [coord[0] for coord in r]
    x = [coord[1] for coord in r]
    fig.add_scatter3d(x=x,
                      y=y,
                      z=Z[y, x],
                      mode='markers',
                      marker=dict(
                          size=1.5,
                          color='blue',
                          opacity=.0))
    return fig

def visualise_trench(trench_cells, Z):
    fig = go.Figure(data=[go.Surface(z=Z, colorscale='Viridis', showscale=True)])
    dict_Contours = extract_contours(Z)
    for trench_cell in trench_cells:
        trench = Extract_Trench.extract_trench(Z, dict_Contours, cell = trench_cell)
        if type(trench) != None:
            trench_x, trench_y = trench[:,0].astype(np.uint64), trench[:,1].astype(np.uint64)
            fig.add_scatter3d(x=trench_y,
                              y=trench_x,
                              z=Z[trench_x, trench_y],
                              mode='markers',
                              marker=dict(
                                  size=1.5,
                                  color='yellow',
                                  opacity=.0))
    fig.update_layout(title='Spring Valley Farm Trenches 3D Plot', autosize=True,
                      margin=dict(l=65, r=50, b=65, t=90))
    return fig


if __name__ == '__main__':
    # Make the interpolation
    # BASE_DIR = '../../../Archive/backend/data/Spring_Valley/sv_rect.csv'
    # data = load_file(BASE_DIR)
    #
    # w, h = 400, 800
    # X, Y, Z = generate_grid(data, w, h)

    # np.savetxt('sv_400x800_Z.csv', Z)

    Z = np.loadtxt('sv_400x800_Z.csv')
    # Z = np.loadtxt('./Backend/Optimization/tree_plan/sv_400x800_Z.csv')
    """pycharm_path = '../../Data/mt_ainslie.txt'
    path_from_root = 'Backend/Data/mt_ainslie.txt'
    Z = load_dem_data(pycharm_path, path_from_root)"""

    n = 1
    creek_slope, coordinates_array, trench_cells = find_trenches(Z, 3, n)
    print(trench_cells)
    # plt = plot_result(Z, t, "Trenches", 1, 'gray')
    # plt = plot_result(Z, t, "Trenches", 2, 'yellow')
    fig = visualise_trench(trench_cells, Z)
    fig = visualise_creek(Z, r=coordinates_array, fig=fig)
    fig.show()
    # plt = display_trench(trench, Z)
    # plt = plot_creek(Z, coordinates_array, "Trenches "+str(n)+" step(s) away", creek_slope)
    # display_contours(dict_Contours, Z)
    # plt.show()
