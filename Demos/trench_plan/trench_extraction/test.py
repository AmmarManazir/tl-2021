import sys, os
sys.path.insert(1, os.getcwd())
import numpy as np
from Backend.trench_plan import Extract_Trench, Extract_Contours

if __name__ == "__main__":
    """
        -- Notes for tester: install 'shapely' and 'skimage' via pip
    """

    pycharm_path = '../../Data/spring_valley.txt'
    path_from_root = 'Backend/Data/spring_valley.txt'
    Z = np.loadtxt(pycharm_path if os.path.exists(pycharm_path) else path_from_root)

    dict_Contours = Extract_Contours.extract_contours(Z)  
    Extract_Contours.display_contours(dict_Contours, Z)
    test_cells = []
    test_cells.append([100, 100])
    test_cells.append([135, 146])
    test_cells.append([85, 146])
    test_cells.append([268, 100])
    test_cells.append([25, 85])
    test_cells.append([50, 50])

    for test_cell in test_cells:
        trench = Extract_Trench.extract_trench(Z, dict_Contours, cell = test_cell)
        Extract_Trench.display_trench(trench, Z, cell=test_cell)