# Trench Extraction. 

## Contributer
* Yasar Adeel Ansari
  
The contours that you see in the photo bellow form the Digital Elevation Model of the Farm/ Land. 

## What are Contours and Trenches? 
We refer to trenches as a contour that will be dug across in order to curb the flow of water into the gully. So some of the water actually actually gets trapped before moving across the trench. 

The process of extracting a trench can be devided into 2 functons. 

1.  **Extract_Contours** 
    * ***Features***
        * Extract all contours based on the elevation. 
        * Extracted contours are fully connected.  
    * ***Inputs***
        * *Grid* : Array (n x m) 
            * Represents the actual Height Map or the DEM(Digital Elevation Model) of the form. 
    * ***Outputs***
        * *dict_Contours*: Dictionary
            * Each key of the dictionary is a number that represents some elevation in the land. And each.elevation has multiple contours that have that same elevation as its values. 
    * ***Visualisation***: 
        * Each continuous yello curve is a contour. 
        *  <img src="contours.png" width="200">
2. **Extract_Trenches**
    * ***Features***
        * Identify and differentiate between contours at the same height. 
        * If given point lies on the contour's perimeter, return contour as trench. 
        * Find nearest contour that contains the point if it lies between the perimeter of 2 contours. 
        * Compensate for the contour information lost when converting vector to mask(grid). 
  
    * ***Inputs***
        * *cell* : Tuple or List with 2 elements.
            * coordinates of the cell with high accumulation. 
        * *dict_Contours:* Dictionary
            * Each key of the dictionary is a number that represents some elevation in the land. And each elevation has multiple contours that have that same elevation as its values. 
        * *Grid:* Array (n x m) 
            * Represents the actual Height Map or the DEM(Digital Elevation Model) of the form. 
    * ***Outputs***:
        * *trench* : Array(n x 2)
            * The coordinates of the contour that need to be trenched. 
  
    * ***Visualisation***
        * The blue point represents a point. We want to trench the contour that this blue point lies on. (yellow curve)
        * <img src="trench_ex.png" width="220">

## Evaluation
* Time taken on Mac Book Pro 2019
  
   Function       | extract contours | extract trench
  -------------   |  -------------   | ----------
  Time(seconds) taken on (800 x 400 Grid)| 0.03 - 0.5 | 0.0002 - 0.005 
* To Do: 
  *  add time taken to implement on the server.