import sys, os
sys.path.insert(1, os.getcwd())
from Backend.trench_plan.evenly_spaced_trenches import trench_plan
from Demos.ridge_and_gully.ridge_detection.ridge_detection import plot_result
import numpy as np
# from mayavi import mlab     # Mayavi requires python 3.7, vtk 8.1.2, and is incompatible with opencv-python

if __name__ == "__main__":

    pycharm_path = '../../Demos/Data/spring_valley.txt'
    if os.path.exists(pycharm_path):
        Z = np.loadtxt(pycharm_path)
    else:
        Z = np.loadtxt('Backend/Data/spring_valley.txt')

    t = trench_plan(Z, num_trenches=10)
    plot_result(Z, t, "Evenly spaced trenches")

    # mlab.surf(Z, colormap = 'gist_earth')
    # mlab.view(20)
    # mlab.show()