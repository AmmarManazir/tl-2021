# author: Yasar Adeel Ansari
# The bellow code takes the following parameters.
# In[0]
import numpy as np
from scipy.interpolate import griddata
from scipy import interpolate 
import cv2
import sys 
import matplotlib.pyplot as plt
if "./Backend/Analysis/ridge_detection" not in sys.path:
        sys.path.append("./Backend/Analysis")
        from useful_funcs import generateGrid 
"""
Input:  
    Grid : Array (n x m) 
        - The DEM of the farm/ land
    Trench_Cell : Tuple (x, y)
        - Pixel coordinates of a cell (inside the Grid) (that is part of a gully). 
Output:
    Trench: List with 'n' elements. 
        - An array of coordinates that represent a closed perimeter being drawn like a contour.
"""   
# In[3]
def generateTrenchfromCell(Grid, Trench_Cell = [420, 300]):
    print(Grid.min(), Grid.max())
    Grid = np.nan_to_num(Grid, nan = -1.0)
    height = Grid[Trench_Cell[0], Trench_Cell[1]]
    binary = np.zeros((Grid.shape[0] * Grid.shape[1]))
    coords_similar_elev = np.where(abs(Grid.astype(np.uint64).ravel() - height) < 1)[0]
    binary[coords_similar_elev] = 255
    Binary_Grid = binary.reshape(Grid.shape).astype(np.uint8)
    axis_coordinates = lambda size : np.linspace(0, 1, size)
    contourfunc = interpolate.interp2d(axis_coordinates(Grid.shape[1]), axis_coordinates(Grid.shape[0]), Binary_Grid)
    Binary_Grid = contourfunc(axis_coordinates(Grid.shape[1]), axis_coordinates(Grid.shape[0])).astype(np.uint8)
    plt.imshow(Binary_Grid)
    plt.show()
    Contours, hierarchy = cv2.findContours(Binary_Grid, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    print(hierarchy)
    dists = []
    centroids = []
    for contour in Contours:
        moments = cv2.moments(contour)
        centroid = np.array([int(moments['m10']/moments['m00']), int(moments['m01']/moments['m00'])]) 
        centroids.append(centroid)
        dists.append(np.linalg.norm(centroid - np.array(Trench_Cell)))
    idx = np.argmin(np.array(dists))
    contour_Trench = Contours[idx]
    epsilon = 0.1 * cv2.arcLength(contour_Trench, True)
    centroid_Trench = centroids[idx]
    bounds_Trench = cv2.approxPolyDP(contour_Trench, epsilon, True)
    hull = cv2.convexHull(bounds_Trench, returnPoints = True)
    print("hull" + str(hull))
    threshold = - 1
    print("centroid ------- " + str(centroid_Trench))
    for bound in hull:
        dist_boundtocentroid = np.linalg.norm(bound[0] - centroid_Trench)
        if dist_boundtocentroid > threshold:
            threshold = dist_boundtocentroid
    coords_similar_elev_2d = list(zip(*np.where(abs(Grid.astype(np.uint64) - height) < 1)))
    Trench = np.zeros((Grid.shape[0], Grid.shape[1]))
    for coord in coords_similar_elev_2d:
        if np.linalg.norm(coord - centroid_Trench) <= threshold:
            Trench[coord[0], coord[1]] = 255
    plt.imshow(Trench)
    plt.show()
# In[1]
if __name__ == "__main__":
    PATH = './Archive/Backend/data/Spring_Valley/'
    Data = np.loadtxt(PATH + "/sv_rect.csv", delimiter=',')
    # Interpolate the csv into numpy array
    w, h = 400, 800
    X, Y, Z = generateGrid(Data, w, h)
    generateTrenchfromCell(Z)
# In[2]
