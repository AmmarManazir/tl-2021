import sys, os
sys.path.insert(1, os.getcwd())
import numpy as np
from Backend.trench_plan.Extract_Contours import extract_contours
import matplotlib.pyplot as plt
from matplotlib.patches import Circle

def display_contours(dict_Contours, Grid):
    Z = Grid.astype(np.uint64)
    Binary_Grid = np.zeros_like(Z)
    for height in range(Z.min(), Z.max()):
        height_Contours = dict_Contours[str(height)]
        for height_Contour in height_Contours:
            id_x = height_Contour[:,0].astype(np.uint64)
            id_y = height_Contour[:,1].astype(np.uint64)
            Binary_Grid[id_x, id_y] = 255
    plt.imshow(Binary_Grid)
    plt.show()

def display_trench(trench, Grid, cell = [420, 300]):
    Binary_Grid = np.zeros_like(Grid)
    if type(trench) != None:
        trench_x, trench_y = trench[:,0].astype(np.uint64), trench[:,1].astype(np.uint64)
        print(len(trench))
    Binary_Grid[trench_x, trench_y] = 255
    fig, ax = plt.subplots(1)
    ax.set_aspect('equal')
    ax.imshow(Binary_Grid)
    trench_cell_fig = Circle((cell[1], cell[0]), 10)
    ax.add_patch(trench_cell_fig)
    plt.show()

if __name__ == "__main__":
    pycharm_path = '../../Demos/Data/spring_valley.txt'
    path_from_root = 'Backend/Data/spring_valley.txt'
    Z = np.loadtxt(pycharm_path if os.path.exists(pycharm_path) else path_from_root)

    dict_Contours = extract_contours(Z)
    display_contours(dict_Contours, Z)