from Backend.trench_plan.step_terrain import trench_plan, extract_main_creek, diff_ratios, obtain_rank, select_coords, extract_contours, extract_trench
from Demos.ridge_and_gully.visualize_accumulation import load_dem_data
import plotly.graph_objects as go
from Backend.ridge_and_gully.pysheds_accumulation import water_accumulation
import numpy as np
import matplotlib.pyplot as plt

def main_creek(Z):
    """returns just the main creek for visualisation"""
    acc = water_accumulation(Z)
    main_creek_coords = extract_main_creek(acc)
    creek_bools = np.zeros(Z.shape, dtype=bool)
    creek_bools[tuple(np.array(main_creek_coords).T)] = True
    return creek_bools


def heights_and_ratios(Z, step_size=1):
    """returns just the heights and ratios for 2D matplotlib visualisation"""
    acc = water_accumulation(Z)
    main_creek_coords = extract_main_creek(acc)
    heights = [Z[c] for c in main_creek_coords]
    accumulation = [acc[c] for c in main_creek_coords]
    ratios = diff_ratios(heights, step_size)
    ranked_ratios = obtain_rank(ratios)
    normalised_ratios = [ranked_ratios[i]/len(ranked_ratios) for i in range(len(ranked_ratios))]
    return heights, normalised_ratios

def test_synthetic_data():
    demo1 = [10,9,9,8,5,4,3,2,1]
    demo2 = [10, 9, 8, 6, 5, 4, 2, 1, 0]
    demo3 = [10, 8, 8, 9, 8, 7, 5, 4, 4]
    demo4 = [10, 8, 9, 8, 7, 4, 3, 3]
    demos = [demo1,demo2,demo3,demo4]
    for demo in demos:
        ratios = diff_ratios(demo,2)
        plot_ratios(demo,ratios)

def visualise_creek_2d(Z, num_trenches=3, title="Main creek"):
    t = trench_plan(Z, num_trenches)
    c = main_creek(Z)
    plt.imshow(Z)
    plt.title(title)
    y, x = np.where(t == 1)
    plt.scatter(x, y, marker='.', linewidths=.1, c='orange')
    y, x = np.where(c == 1)
    plt.scatter(x, y, marker='.', linewidths=.1, c='yellow')
    # plt.show()
    return plt

def plot_cross_section(Z, step_size=1, title="Cross section"):
    heights, ratios = heights_and_ratios(Z, step_size)
    return plot_ratios(heights, ratios, title)

def test_step_sizes(Z):
    step_sizes = range(2,20)
    for step_size in step_sizes:
        plot_cross_section(Z, step_size)

def visualise_creek(Z, fig):
    acc = water_accumulation(Z)
    main_creek_coords = extract_main_creek(acc)
    y = [coord[0] for coord in main_creek_coords]
    x = [coord[1] for coord in main_creek_coords]
    fig.add_scatter3d(x=x,
                      y=y,
                      z=Z[y, x],
                      mode='markers',
                      marker=dict(
                          size=1.5,
                          color='blue',
                          opacity=.0))
    return fig

def visualise_trench(Z, num_trenches=3):
    selected_coords = select_coords(Z, num_trenches)
    fig = go.Figure(data=[go.Surface(z=Z, colorscale='Viridis', showscale=True)])
    dict_Contours = extract_contours(Z)
    for trench_cell in selected_coords:
        trench = extract_trench(Z, dict_Contours, cell = trench_cell)
        if type(trench) != None:
            trench_x, trench_y = trench[:,0].astype(np.uint64), trench[:,1].astype(np.uint64)
            fig.add_scatter3d(x=trench_y,
                              y=trench_x,
                              z=Z[trench_x, trench_y],
                              mode='markers',
                              marker=dict(
                                  size=1.5,
                                  color='yellow',
                                  opacity=.0))
    fig.update_layout(title='Spring Valley Farm Trenches 3D Plot', autosize=True,
                      margin=dict(l=65, r=50, b=65, t=90))
    return fig

def plot_ratios(heights, ratios, title="Trench selection"):
    """plots distance against height, coloured by the ratio used for calculating step terrain"""
    im = plt.scatter(range(len(heights)),heights, c=ratios)
    plt.title(title)
    plt.xlabel("Distance (m)")
    plt.ylabel("Height (m)")
    plt.colorbar(im)
    return plt

if __name__ == '__main__':
    pycharm_path = '../Data/spring_valley.txt'
    path_from_root = 'Backend/Data/spring_valley.txt'
    Z = load_dem_data(pycharm_path, path_from_root)

    # fig = visualise_trench(Z, 3)
    # fig = visualise_creek(Z,fig=fig)
    # fig.show()
    t = trench_plan(Z)
    print(np.where(t))

    # test_synthetic_data()
    visualise_creek_2d(Z).show()
    plot_cross_section(Z).show()
    # test_step_sizes(Z)


