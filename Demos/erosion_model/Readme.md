# Erosion model
The erosion model is used to simulate what happens to the land over time. We planned to use this to verify how well our plans reduce erosion, however we agreed with our client that this is no longer a priority so this is currently a stretch goal.

Here is an example of the erosion model in action:
![Erosion Model](../../Archive/Sem1/backend/simulations/erosion.gif)