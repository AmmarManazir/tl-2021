# In[0]
import os
from os import walk
import sys
import numpy as np
from scipy.interpolate import griddata
import matplotlib.pyplot as plt
from matplotlib.colors import LightSource
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from mayavi import mlab
from PIL import Image
import pandas as pd
# In[1]
class Droplet:
    def __init__(self, position, grid):
        self.position = position
        self.direction = np.zeros(2)
        self.velocity = 1.0
        self.water_volume = 1.0  
        self.sediment = 0.0
        self.carry_capacity = 0.0
        self.height = None
        
def get_gradient(x, y, u, v, grid):
    grad_x = ((grid[x + 1, y] - grid[x, y]) * 1 - v) + ((grid[x + 1, y + 1] - grid[x, y + 1]) * v)
    grad_y = ((grid[x, y + 1] - grid[x, y]) * 1 - u) + ((grid[x + 1, y + 1] - grid[x + 1, y]) * u)
    gradient = np.array([grad_x, grad_y])
    return gradient

def get_direction(gradient, direction, inertia):
    direction = direction * inertia - gradient * (1 - inertia)
    return direction/np.linalg.norm(direction)

def get_interpolated_height(x, y, u, v, grid):
    height = grid[x - 1, y + 1] * (1 - u) * (1 - v) + grid[x + 1, y + 1] * u * (1 - v) + grid[x - 1, y - 1] * (1 - u) * v + grid[x - 1, y + 1] * v * u
    return height
         
def get_gaussian_filter(radius, std = 1):
    n = radius
    vector = np.linspace(-(n - 1) / 2., (n - 1) / 2., n)
    xx, yy = np.meshgrid(vector, vector)
    kernel = np.exp(-0.5 * (np.square(xx) + np.square(yy)) / np.square(std))
    kernel = kernel/kernel.sum()
    return kernel

def visualise_Terrain(X, Y, Z):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection = '3d')
    ls = LightSource(270, 45)
    rgb = ls.shade(Z, cmap=cm.gist_earth, vert_exag=0.1, blend_mode='soft')
    surf1 = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, color='0.99', antialiased=True, edgecolor='0.5')

# In[2]
@mlab.animate(delay = 100)
def perform_erosion(grid):
    f = mlab.gcf()
    epochs = 3000000
    cycles = 50
    gravity = 3
    carry_capacity_param = 16
    min_slop = 0.01
    deposition_speed = 1
    radius = 8
    erosion_speed = 0.3
    evaporation_speed = 0.1
    inertia = 0.3
    w = grid.shape[0] - radius * 2
    h = grid.shape[1] - radius * 2
    Erosin_Counts = np.zeros_like(grid)

    for epoch in range(epochs):
        position_x = np.random.randint(radius, w)
        position_y = np.random.randint(radius, h)
        position = np.array([position_x, position_y])
        coord_x = position_x
        coord_y = position_y
        drop = Droplet(position, grid)
        cell_coord_x = drop.position[0] - coord_x
        cell_coord_y = drop.position[1] - coord_y
        drop.height = get_interpolated_height(coord_x, coord_y, cell_coord_x, cell_coord_y,  grid)

        for cycle in range(cycles):
            
            gradient = get_gradient(coord_x, coord_y, cell_coord_x, cell_coord_y, grid)
            drop.direction = get_direction(gradient, drop.direction, inertia)
            if not(w > coord_x > radius) or not(h > coord_y > radius) or (drop.direction[0] == 0.0 and drop.direction[1] == 0.0):
                break

            new_pos = drop.position + drop.direction
            new_height = get_interpolated_height(int(new_pos[0]), int(new_pos[1]), new_pos[0] - int(new_pos[0]), new_pos[1] - int(new_pos[1]), grid)
            delta_height = new_height - drop.height
            drop.carry_capacity = max(-delta_height, min_slop) * drop.velocity * carry_capacity_param * drop.water_volume

            if drop.sediment > drop.carry_capacity or delta_height > 0:
                if delta_height > 0:
                    deposit_at_pos = min(delta_height, drop.sediment)
                else:
                    deposit_at_pos = (drop.sediment - drop.carry_capacity) * deposition_speed

                drop.sediment -= deposit_at_pos
                grid[coord_x - 1, coord_y + 1] += deposit_at_pos * (1 - cell_coord_x) * (1 - cell_coord_y)
                grid[coord_x + 1, coord_y + 1] += deposit_at_pos * cell_coord_x * (1 - cell_coord_y)
                grid[coord_x - 1, coord_y - 1] += deposit_at_pos * (1 - cell_coord_x) * cell_coord_y
                grid[coord_x + 1, coord_y - 1] += deposit_at_pos * cell_coord_x * cell_coord_y

            else:
                erosion_at_pos = min((drop.carry_capacity - drop.sediment) * erosion_speed, -delta_height)
                
                xx = int(drop.position[0])
                yy = int(drop.position[1])

                Erosin_Counts[xx, yy] = Erosin_Counts[xx, yy] + erosion_at_pos

                weights = get_gaussian_filter((radius * 2) + 1)
                erosion_at_poses = erosion_at_pos * weights
                pixels_to_erode = grid[coord_x - radius: coord_x + radius + 1, coord_y - radius: coord_y + radius + 1]

                for i in range((2 * radius) + 1):
                    for j in range((radius * 2) + 1):
                        if erosion_at_poses[i, j] > pixels_to_erode[i, j]:
                            erosion_at_poses[i, j] = pixels_to_erode[i, j]
                pixels_to_erode -= erosion_at_poses
                drop.sediment += erosion_at_poses.sum()
                
           
            drop.velocity = np.sqrt(drop.velocity ** 2 + delta_height * gravity)
            drop.water_volume = drop.water_volume * (1 - evaporation_speed)

            if type(drop.velocity) != float:
                break
            
            drop.position = new_pos
            drop.height = new_height
            coord_x = int(drop.position[0])
            coord_y = int(drop.position[1])
            cell_coord_x = drop.position[0] - coord_x
            cell_coord_y = drop.position[1] - coord_y

        if epoch % 1000 == 0:
            s.mlab_source.scalars = grid
            yield
    return Erosin_Counts
    
# In[3]
if __name__ == "__main__":
    pycharm_path = '../Data/spring_valley.txt'
    path_from_root = 'Backend/Data/spring_valley.txt'
    Z = np.loadtxt(pycharm_path if os.path.exists(pycharm_path) else path_from_root)
# In[5]
    s = mlab.surf(Z, colormap = 'gist_earth')
    Erosion_Counts = perform_erosion(Z)
    mlab.view(20)
    mlab.show()
    mlab.close()
# %%
    movie_path = '/Users/Uni/civlise.ai-techlauncher-2020/Images/erosion/movie003'
    files = []
    for (dirpath, dirnames, filenames) in walk(movie_path):
        files.append(filenames)
    image_files = sorted(files[0])
    image_files = image_files[1:]
    
    frames = []
# %%
    for image_file in image_files:
        frame = Image.open(movie_path + '/' +  image_file)
        frames.append(frame)
    frames[0].save('moving_balls.gif', format='GIF', append_images=frames[1:], save_all=True, duration=30, loop=0)
     #  %%
