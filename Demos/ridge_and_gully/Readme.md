## Ridges and gullies

### What is a ridge?
According to Wikipedia, A ridge or a mountain ridge is a geographical feature consisting of a chain of 
mountains or hills that form a continuous elevated crest for some distance. So basically, if we recognize a 
height terrain map as an image, ridge is that pixels that have great descent gradient with its neighbour pixels.

### What is the gully?
A gully is a landform created by running water, eroding sharply into soil, typically on a hillside. 
Gullies resemble large ditches or small valleys, but are metres to tens of metres in depth and width.

### Why is ridge and gully detection useful in our project?
* Ridge and gully detection helps to analyse water flow on the property, so farmer's know where their water is going when it rains. 
* Ridges are are also frequently used for transportation. They are often the most cost effective places to build roads because less landscaping is required and less erosion occurs.
 
### Implementing a method
- Use water accumulation to find gullies, and do the same thing on an upside-down DEM to find ridges.
- Use image processing feature detection
- Use path finding to connect ridges to generate a road plan
