from Backend.ridge_and_gully.pysheds_accumulation import closed_ridges, closed_gullies
import plotly.graph_objects as go
import matplotlib.pyplot as plt
import numpy as np
import sys, os

if __name__ == "__main__":
    pycharm_path = '../../Demos/Data/spring_valley.txt'
    path_from_root = 'Backend/Data/spring_valley.txt'
    Z = np.loadtxt(pycharm_path if os.path.exists(pycharm_path) else path_from_root)

    threshold = 99
    g = closed_gullies(Z, threshold)
    r = closed_ridges(Z, threshold)

    plt.imshow(Z)
    plt.title("Pysheds accumulation with threshold: {}".format(threshold))
    y, x = np.where(g == 1)
    plt.scatter(x, y, marker='.', linewidths=.1, c='blue')

    y, x = np.where(r == 1)
    plt.scatter(x, y, marker='.', linewidths=.1, c='red')

    fig = go.Figure(data=[go.Surface(z=Z, colorscale='Viridis', showscale=True)])

    fig.update_traces(contours_z=dict(show=True, usecolormap=True,
                                      highlightcolor="limegreen", project_z=True))

    fig.add_scatter3d(x=np.where(g == True)[1],
                      y=np.where(g == True)[0],
                      z=Z[g == True]+3,
                      mode='markers',
                      marker=dict(
                          size=1,
                          color='blue',
                          opacity=.8
                      ))

    fig.add_scatter3d(x=np.where(r == True)[1],
                      y=np.where(r == True)[0],
                      z=Z[r == True]+3,
                      mode='markers',
                      marker=dict(
                          size=1,
                          color='red',
                          opacity=.8
                      ))

    fig.update_layout(title='Spring Valley Farm 3D Plot with threshold {}'.format(threshold), autosize=True,
                      margin=dict(l=65, r=50, b=65, t=90))

    fig.show()