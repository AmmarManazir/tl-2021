import matplotlib.pyplot as plt
import time
import numpy as np
from Backend.ridge_and_gully.peak_connections import peak_connections
import os

if __name__ == "__main__":
    property = "spring_valley"
    pycharm_path = f'../../../Demos/Data/{property}.txt'
    path_from_root = f'Demos/Data/{property}.txt'
    Z = np.loadtxt(pycharm_path if os.path.exists(pycharm_path) else path_from_root)

    start = time.time()
    connected_peaks = peak_connections(Z, peak_size=10, downscale=1)
    end = time.time()
    print(f"Took {end-start} seconds to connect peaks")
    x, y = np.where(connected_peaks)

    plt.imshow(Z)
    plt.scatter(y, x)
    plt.show()