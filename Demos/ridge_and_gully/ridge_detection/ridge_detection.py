import cv2
import numpy as np
import pandas as pd
from scipy import signal
from scipy.interpolate import griddata
import matplotlib.pyplot as plt
import plotly.graph_objects as go

from Backend.ridge_and_gully.pysheds_accumulation import closure, water_accumulation


def load_file(file_path):
    data = pd.read_csv(file_path, header=None)
    data = np.array(data, dtype=np.float)
    return data


def adaptive_filter(I, iter=3, h=1.5):
    Gx = np.array([[-np.sqrt(2) / 4, 0, np.sqrt(2) / 4],
                   [-1, 0, 1],
                   [-np.sqrt(2) / 4, 0, np.sqrt(2) / 4]])
    Gy = np.array([[np.sqrt(2) / 4, 1, np.sqrt(2) / 4],
                   [0, 0, 0],
                   [-np.sqrt(2) / 4, -1, -np.sqrt(2) / 4]])
    result = np.copy(I)
    for i in range(iter):
        pad_I = cv2.copyMakeBorder(result, 1, 1, 1, 1, cv2. BORDER_REPLICATE)
        H, W = pad_I.shape
        gx = signal.convolve2d(pad_I, Gx, mode='same', boundary='symm')
        gy = signal.convolve2d(pad_I, Gy, mode='same', boundary='symm')
        dxy = np.sqrt(np.square(gx) + np.square(gy))
        wxy = np.exp(-(np.sqrt(dxy)) / (2 * np.square(h)))
        for i in range(1, H-1):
            for j in range(1, W-1):
                N = np.sum(wxy[i-1:i+2, j-1:j+2])
                result[i-1, j-1] = np.sum(pad_I[i-1:i+2, j-1:j+2]*wxy[i-1:i+2, j-1:j+2]) / N
    return result


def generate_grid(points, width=1024, height=2048):
    z = points[:, 2]

    # Define the grid
    start_point = np.min(points, axis=0)
    end_point = np.max(points, axis=0)

    # Use meshgrid to generate a grid map, flip y because the latitude
    X, Y = np.meshgrid(np.linspace(start_point[0], end_point[0], width+2),
                       np.linspace(end_point[1], start_point[1], height+2))

    # Use interpolate to interpolate unstructured D-D data
    Z = griddata(points[:, 0:2], z, (X, Y), method='cubic')

    return X[1:-1, 1:-1], Y[1:-1, 1:-1], Z[1:-1, 1:-1]


def steepest_ascent_method(Z, threshold=5):
    """
    :param Z: the elevation values of a mxn cells
    :param threshold: the threshold value to decide whether a cell is a ridge
    :return: (array[coordinate of x], array[coordinate of y])
    """
    # Add border to the default height map
    wrap_Z = cv2.copyMakeBorder(Z, 1, 1, 1, 1, borderType=cv2.BORDER_REFLECT)

    # Initialization
    counts = np.zeros(wrap_Z.shape, dtype=np.uint8)

    # Steepest_ascent_method
    for i in range(1, wrap_Z.shape[0] - 1):
        for j in range(1, wrap_Z.shape[1] - 1):
            # Calculate maximum inclined direction from eight directions
            new_i, new_j = i, j
            hasAscent = True
            count = 0
            while hasAscent and count < 10:
                if new_i <= 0 or new_i >= Z.shape[0]-1 or new_j <= 0 or new_j >= Z.shape[1]-1:
                    hasAscent = False
                else:
                    ascent_matrix = Z[new_i - 1:new_i + 2, new_j - 1:new_j + 2] - Z[new_i, new_j]
                    steepest_ascent = np.max(ascent_matrix)
                    if steepest_ascent > 0:
                        r = np.where(ascent_matrix == steepest_ascent)
                        q, p = new_i + r[0][0] - 1, new_j + r[1][0] - 1
                        counts[q, p] += 1
                        new_i, new_j = q, p
                        count += 1
                    else:
                        hasAscent = False

    counts = counts[1:-1, 1:-1]
    result = np.vectorize(lambda a: 1 if a > threshold else 0)(counts)
    return np.array(result, dtype=np.uint8), counts


def steepest_descent_method(Z, threshold=5):
    """
    :param Z: the elevation values of a mxn cells
    :param threshold: the threshold value to decide whether a cell is a gully
    :return: (array[coordinate of x], array[coordinate of y])
    """
    # Add border to the default height map
    wrap_Z = cv2.copyMakeBorder(Z, 1, 1, 1, 1, borderType=cv2.BORDER_REFLECT)

    # Initialization
    counts = np.zeros(wrap_Z.shape, dtype=np.uint8)

    # Steepest_ascent_method
    for i in range(1, wrap_Z.shape[0] - 1):
        for j in range(1, wrap_Z.shape[1] - 1):
            # Calculate maximum inclined direction from eight directions
            new_i, new_j = i, j
            hasDescent = True
            count = 0
            while hasDescent and count < 10:
                if new_i <= 0 or new_i >= Z.shape[0]-1 or new_j <= 0 or new_j >= Z.shape[1]-1:
                    hasDescent = False
                else:
                    descent_matrix = Z[new_i - 1:new_i + 2, new_j - 1:new_j + 2] - Z[new_i, new_j]
                    steepest_descent = np.min(descent_matrix)
                    if steepest_descent < 0:
                        r = np.where(descent_matrix == steepest_descent)
                        q, p = new_i + r[0][0] - 1, new_j + r[1][0] - 1
                        counts[q, p] += 1
                        new_i, new_j = q, p
                        count += 1
                    else:
                        hasDescent = False

    counts = counts[1:-1, 1:-1]
    result = np.vectorize(lambda a: 1 if a > threshold else 0)(counts)
    return np.array(result, dtype=np.uint8), counts


def plot_result(Z, r, title):
    y, x = np.where(r == 1)
    plt.imshow(Z)
    plt.scatter(x, y, marker='.', linewidths=.1, c='gray')
    plt.title(title)
    plt.show()


def find_end_node(r, g):
    _r = cv2.copyMakeBorder(r, 1, 1, 1, 1, cv2.BORDER_CONSTANT, 0)
    _g = cv2.copyMakeBorder(g, 1, 1, 1, 1, cv2.BORDER_CONSTANT, 0)
    r_end_nodes = []
    g_end_nodes = []
    for i in range(1, _r.shape[0]-1):
        for j in range(1, _r.shape[1]-1):
            if 2 < i < _r.shape[0]-2 and \
               2 < j < _r.shape[1]-2 and \
               np.mean(_r[i-1:i+2, j-1:j+2]) + np.mean(_g[i-1:i+2, j-1:j+2]) <= 1/3:
                if _r[i, j] == 1:
                    r_end_nodes.append([i-1, j-1])
                if _g[i, j] == 1:
                    g_end_nodes.append([i-1, j-1])

    return np.array(r_end_nodes), np.array(g_end_nodes)


def evaluation_metric(Z):
    _Z = cv2.copyMakeBorder(Z, 1, 1, 1, 1, cv2.BORDER_REFLECT)
    rise_sum = np.zeros(Z.shape)
    r = np.zeros(Z.shape)
    g = np.zeros(Z.shape)
    for i in range(1, _Z.shape[0]-1):
        for j in range(1, _Z.shape[1]-1):
            # Calculate slope
            dx = ((_Z[i-1, j+1] + 2 * _Z[i, j+1] + _Z[i+1, j+1]) - (_Z[i-1, j-1] + 2 * _Z[i, j-1] + _Z[i+1, j-1])) / (8 * 10)
            dy = ((_Z[i+1, j-1] + 2 * _Z[i+1, j] + _Z[i+1, j+1]) - (_Z[i-1, j-1] + 2 * _Z[i-1, j] + _Z[i-1, j+1])) / (8 * 10)
            rise_sum[i-1, j-1] = np.sqrt(np.square(dx) + np.square(dy))
            # Calculate mean elevation
            r[i - 1, j - 1] = Z[i - 1, j - 1] - np.mean(_Z[i - 1:i + 2, j - 1:j + 2])
            g[i - 1, j - 1] = -r[i-1, j-1]

    # Normalization
    rise_sum = (rise_sum - np.min(rise_sum)) / (np.max(rise_sum) - np.min(rise_sum))
    r = (r - np.min(r)) / (np.max(r) - np.min(r))
    g = (g - np.min(g)) / (np.max(g) - np.min(g))
    rise_sum = 1 - rise_sum

    # Generate metric
    r_metric = 2 * (rise_sum * r) / (rise_sum + r)
    g_metric = 2 * (rise_sum * g) / (rise_sum + g)
    return r_metric, g_metric


def connect(r_metric, g_metric, r, g):
    # Initialization, decide what nodes to close
    r_nodes, g_nodes = find_end_node(r, g)
    while True:
        new_r_nodes = []
        for node in r_nodes:
            i, j = node[0], node[1]
            if i < 1 or i > r.shape[0]-2 or j < 1 or j > r.shape[1]-2:
                continue
            if np.mean(r[i-1:i+2, j-1:j+2]) + np.mean(g[i-1:i+2, j-1:j+2]) > 1/3:
                pass
            else:
                flatten_metric = r_metric[i-1:i+2, j-1:j+2].flatten()
                flatten_r = r[i-1:i+2, j-1:j+2].flatten()
                max_metric = 0
                max_index = 0
                for k in range(9):
                    # Should not go to exist ridge or gully
                    if flatten_r[k] == 1:
                        continue
                    if flatten_metric[k] > max_metric:
                        max_metric = flatten_metric[k]
                        max_index = k
                new_index_i = i + max_index // 3 - 1
                new_index_j = j + max_index % 3 - 1
                r[new_index_i, new_index_j] = 1
                new_r_nodes.append([new_index_i, new_index_j])
        new_g_nodes = []
        for node in g_nodes:
            i, j = node[0], node[1]
            if i < 1 or i > g.shape[0]-2 or j < 1 or j > g.shape[1]-2:
                continue
            if np.mean(r[i-1:i+2, j-1:j+2]) + np.mean(g[i-1:i+2, j-1:j+2]) > 1/3:
                pass
            else:
                flatten_metric = g_metric[i-1:i+2, j-1:j+2].flatten()
                flatten_g = g[i-1:i+2, j-1:j+2].flatten()
                max_metric = 0
                max_index = 0
                for k in range(9):
                    # Should not go to exist ridge or gully
                    if flatten_g[k] == 1:
                        continue
                    if flatten_metric[k] > max_metric:
                        max_metric = flatten_metric[k]
                        max_index = k
                new_index_i = i + max_index // 3 - 1
                new_index_j = j + max_index % 3 - 1
                g[new_index_i, new_index_j] = 1
                new_g_nodes.append([new_index_i, new_index_j])
        if len(new_r_nodes) == 0 and len(new_g_nodes) == 0:
            break
        else:
            r_nodes = np.array(new_r_nodes)
            g_nodes = np.array(new_g_nodes)


if __name__ == "__main__":
    import time
    # Test the runtime
    start = time.process_time()
    # pycharm_path = '../../Data/spring_valley.txt'
    # path_from_root = 'Backend/Data/spring_valley.txt'
    # Z = np.loadtxt(pycharm_path if os.path.exists(pycharm_path) else path_from_root)

    BASE_DIR = '../../../Archive/Sem1/backend/data/Spring_Valley/sv_rect.csv'

    data = load_file(BASE_DIR)

    w, h = 436, 800
    X, Y, Z = generate_grid(data, w, h)

    smooth_Z = adaptive_filter(Z, 3, 1.5)

    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))
    kernel2 = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))

    r_threshold = 120
    r, _r = steepest_ascent_method(smooth_Z, threshold=r_threshold)
    # r = ridges(np.max(Z)-Z, 95)

    g_threshold = 120
    g, _g = steepest_descent_method(smooth_Z, threshold=g_threshold)
    # g = gullies(Z, 95)

    r_metric, g_metric = evaluation_metric(Z)
    acc_r, acc_g = water_accumulation(np.max(Z) - Z), water_accumulation(Z)

    connect(_r, _g, r, g)

    g = cv2.dilate(g, kernel, iterations=1)
    g = cv2.erode(g, kernel, iterations=1)

    r = cv2.dilate(r, kernel, iterations=1)
    r = cv2.erode(r, kernel, iterations=1)

    closure(acc_r, acc_g, r, g)

    end = time.process_time()

    print('Run time is {}'.format(end - start))

    fig = go.Figure(data=[go.Surface(z=Z, colorscale='Viridis', showscale=True)])

    fig.update_traces(contours_z=dict(show=True, usecolormap=True,
                                      highlightcolor="limegreen", project_z=True))

    fig.add_scatter3d(x=np.where(g == 1)[1],
                      y=np.where(g == 1)[0],
                      z=Z[g == 1] + 20,
                      mode='markers',
                      marker=dict(
                          size=2,
                          color='blue',
                          opacity=.5
                      ))

    fig.add_scatter3d(x=np.where(r == 1)[1],
                      y=np.where(r == 1)[0],
                      z=Z[r == 1] + 20,
                      mode='markers',
                      marker=dict(
                          size=2,
                          color='red',
                          opacity=.5
                      ))

    fig.update_layout(title='Spring Valley Farm 3D Plot',
                      scene_aspectmode='manual',
                      scene_aspectratio=dict(x=Z.shape[1] / Z.shape[0], y=1,
                                             z=(np.max(Z) - np.min(Z)) / 10 / Z.shape[0]),
                      margin=dict(l=10, r=10, b=10, t=10))

    fig.show()
