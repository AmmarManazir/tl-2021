import cv2
import os
import numpy as np
import matplotlib.pyplot as plt
from skimage.feature import hessian_matrix, hessian_matrix_eigvals
from Demos.ridge_and_gully.ridge_detection.ridge_detection import adaptive_filter


def detect_ridges(gray, sigma=1.0):
    H_elems = hessian_matrix(gray, sigma=sigma, order='rc')
    maxima_ridges, minima_ridges = hessian_matrix_eigvals(H_elems)
    return maxima_ridges, minima_ridges


def plot_images(*images):
    images = list(images)
    n = len(images)
    fig, ax = plt.subplots(ncols=n, sharey=True)
    for i, img in enumerate(images):
        ax[i].imshow(img, cmap='gray')
        ax[i].axis('off')
    plt.subplots_adjust(left=0.03, bottom=0.03, right=0.97, top=0.97)
    plt.show()


def slope(Z):
    _Z = cv2.copyMakeBorder(Z, 1, 1, 1, 1, cv2.BORDER_REFLECT)
    rise_sum = np.zeros(Z.shape)
    for i in range(1, _Z.shape[0]-1):
        for j in range(1, _Z.shape[1]-1):
            dx = ((_Z[i-1, j+1] + 2 * _Z[i, j+1] + _Z[i+1, j+1]) - (_Z[i-1, j-1] + 2 * _Z[i, j-1] + _Z[i+1, j-1])) / 8
            dy = ((_Z[i+1, j-1] + 2 * _Z[i+1, j] + _Z[i+1, j+1]) - (_Z[i-1, j-1] + 2 * _Z[i-1, j] + _Z[i-1, j+1])) / 8
            rise_sum[i-1, j-1] = np.sqrt(np.square(dx) + np.square(dy))
    rise_sum = (rise_sum - np.min(rise_sum)) / (np.max(rise_sum) - np.min(rise_sum))
    return 1 - rise_sum


def mean_elevation(Z):
    _Z = cv2.copyMakeBorder(Z, 1, 1, 1, 1, cv2.BORDER_REFLECT)
    r = np.zeros(Z.shape)
    g = np.zeros(Z.shape)
    for i in range(1, _Z.shape[0]-1):
        for j in range(1, _Z.shape[1]-1):
            r[i-1, j-1] = Z[i-1, j-1] - np.mean(_Z[i-1:i+2, j-1:j+2])
            g[i-1, j-1] = np.mean(_Z[i-1:i+2, j-1:j+2]) - Z[i-1, j-1]
    r = (r - np.min(r)) / (np.max(r) - np.min(r))
    g = (g - np.min(g)) / (np.max(g) - np.min(g))
    return r, g


if __name__ == "__main__":
    property = "spring_valley"
    pycharm_path = f'../../../Demos/Data/{property}.txt'
    path_from_root = f'Demos/Data/{property}.txt'
    Z = np.loadtxt(pycharm_path if os.path.exists(pycharm_path) else path_from_root)

    Z = adaptive_filter(Z, 5, 5)
    s = slope(Z)
    r, g = mean_elevation(Z)

    sr = 2 * (s * r) / (s + r)
    sg = 2 * (s * g) / (s + g)

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10, 5))
    ax1.imshow(sr)
    ax2.imshow(sg)
    plt.show()

