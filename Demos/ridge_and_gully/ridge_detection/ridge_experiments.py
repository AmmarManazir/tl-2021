"""
author: Yasar
"""
# In[0]
import sys, os

sys.path.insert(1, os.getcwd())
from Backend.ridge_and_gully.pysheds_accumulation import closed_gullies
from Demos.useful_funcs import generateGrid
import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate
from skimage.morphology import local_maxima


# %%
def resize_grayscale(img, shape):
    current_shape = img.shape
    """
    shape is basically (height, width)
    """
    width = current_shape[1]
    height = current_shape[0]
    width_new = shape[1]
    height_new = shape[0]
    axis_coordinates = lambda size : np.linspace(0, 1, size)
    f = interpolate.interp2d(axis_coordinates(width), axis_coordinates(height), img)
    resized_img = f(axis_coordinates(width_new), axis_coordinates(height_new))
    return resized_img

def get_path(s, g, finder, grid):
    start = grid.node(s[0], s[1])
    end = grid.node(g[0], g[1])
    path, runs = finder.find_path(start, end, grid)
    return path

def new_pos(coordinates, current_shape, new_shape):
    current_width, current_height = current_shape
    new_width, new_height = new_shape

    x_coordinates, y_coordinates = coordinates[:,0], coordinates[:,1]

    new_x = (x_coordinates/current_width) * new_width
    new_y = (y_coordinates/current_height) * new_height

    return np.column_stack((new_x.astype(np.int64), new_y.astype(np.int64)))
    

        
# In[1]
if __name__ == "__main__":
    BASE_DIR = './Archive/backend/data/Spring_Valley/sv_rect.csv'
    data = np.loadtxt(BASE_DIR, delimiter=',')
# In[2]
    w, h = int(147) , int(269)
    X, Y, image = generateGrid(data, w, h)
    #image = cv2.GaussianBlur(image, (3,3), cv2.BORDER_DEFAULT)
    print(image.shape)
    
# In[3]

    threshold = 98 
    gullies = closed_gullies(image, threshold)
    plt.imshow(gullies)
    plt.show()
    max_x, max_y = np.where(local_maxima(np.nan_to_num(image)))
    coordinates = np.column_stack((max_x, max_y))

    new_shape = (int(image.shape[0]/4) + 1, int(image.shape[1]/4) + 1)
    coordinates = new_pos(coordinates, image.shape, (new_shape[0] - 1, new_shape[1] - 1))
    image = resize_grayscale(image, new_shape)
    print(image.shape)
# %%
    from pathfinding.core.diagonal_movement import DiagonalMovement
    from pathfinding.core.grid import Grid
    from pathfinding.finder.bi_a_star import BiAStarFinder
    image = np.nan_to_num(image)
    x, y = np.gradient(image)
    g = ((x**2 + y ** 2) ) * 10 + 10   
    matrix = g.T.tolist()
# %%
    paths = []
    print(coordinates.shape)
    print("------------" + str(coordinates.shape))
    span_dists = np.zeros((coordinates.shape[0], coordinates.shape[0]))
    span_paths = dict()
    for i in range(coordinates.shape[0]):
        other_peaks = coordinates.copy()
        #matrix = g.T.tolist()
        grid = Grid(matrix=matrix)
        finder = BiAStarFinder(diagonal_movement=DiagonalMovement.always)
        diff = other_peaks - coordinates[i]
        dists = diff[:,0] ** 2 + diff[:,1] ** 2
        print(dists.min())
        goal_coords = dists.argsort()[1:int(coordinates.shape[0]/2)]
        print(goal_coords)
        for j in goal_coords:
            if span_dists[(i, j)]:
                print("ignoring")
                continue
            goal = coordinates[j]
            path = np.array(get_path(coordinates[i], goal, finder, grid))
            paths.append(path)
            grid = Grid(matrix=matrix)
            finder = BiAStarFinder(diagonal_movement=DiagonalMovement.always)
            cost = np.sum(g[path[:,0], path[:,1]])
            span_dists[i, j] = cost
            span_paths[(i, j)] = path
            span_dists[j, i] = cost
            span_paths[(j, i)] = path
# %%
    plt.imshow(image)
    for path in paths:
        plt.scatter(path[:,1], path[:,0])
    plt.show()
# %%
    from scipy.sparse import csr_matrix
    from scipy.sparse.csgraph import minimum_spanning_tree
    X = csr_matrix(span_dists)
    Tcsr = minimum_spanning_tree(X).toarray().astype(int)
    idx, idy = np.where(Tcsr != 0)
    coords = np.column_stack((idx, idy)).tolist()
    plt.imshow(image)
    for coord in coords:
        i, j = coord
        path = span_paths[(i, j)]
        plt.scatter(path[:,1], path[:,0], color='black')
    plt.show()





