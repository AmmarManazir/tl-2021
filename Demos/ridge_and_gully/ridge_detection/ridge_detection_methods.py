import numpy as np
import os
import cv2
from scipy import signal
import plotly.graph_objects as go


def shape_classification_method(Z):
    result = np.zeros(Z.shape, dtype=np.uint8)

    Z = cv2.copyMakeBorder(Z, 1, 1, 1, 1, cv2.BORDER_REFLECT)
    Gaussian_filter = cv2.getGaussianKernel(5, 15)
    Z = signal.convolve2d(Z, Gaussian_filter, mode='same', boundary='wrap')

    h, w = Z.shape[0], Z.shape[1]
    for i in range(1, h-1):
        for j in range(1, w-1):
            # Z[i,j] is the highest cell among the vertical and horizontal neighbours
            if Z[i, j] > Z[i, j-1] and Z[i, j] > Z[i-1, j] and Z[i, j] > Z[i+1, j] and Z[i, j] > Z[i, j+1]:
                result[i-1, j-1] = 1
            # Z[i, j] is higher than one pair of its vertical or horizontal neighbours and lower than the other pair
            elif (Z[i, j] > Z[i, j-1] and Z[i, j] < Z[i-1, j] and Z[i, j] < Z[i+1, j] and Z[i, j] > Z[i, j+1]) \
                or (Z[i, j] > Z[i-1, j] and Z[i, j] < Z[i, j-1] and Z[i, j] > Z[i+1, j] and Z[i, j] < Z[i, j+1]):
                result[i-1, j-1] = 1
            # Z[i, j] is higher than on of its vertical or horizontal neighbour and lower than the rest three
            elif (Z[i, j] < Z[i-1, j] and Z[i, j] > Z[i+1, j] and Z[i, j] > Z[i, j-1] and Z[i, j] > Z[i, j+1]) \
                or (Z[i, j] < Z[i+1, j] and Z[i, j] > Z[i-1, j] and Z[i, j] > Z[i, j-1] and Z[i, j] > Z[i, j+1]) \
                or (Z[i, j] < Z[i, j-1] and Z[i, j] > Z[i, j+1] and Z[i, j] > Z[i-1, j] and Z[i, j] > Z[i+1, j]) \
                or (Z[i, j] < Z[i, j+1] and Z[i, j] > Z[i, j-1] and Z[i, j] > Z[i-1, j] and Z[i, j] > Z[i+1, j]):
                result[i-1, j-1] = 1
            # Z[i, j] is the highest among its diagonal neighbours
            elif Z[i, j] > Z[i-1, j-1] and Z[i, j] > Z[i-1, j+1] and Z[i, j] > Z[i+1, j-1] and Z[i, j] > Z[i+1, j+1]:
                result[i-1, j-1] = 1
            # Z[i, j] is higher than one pair of its diagonal neighbours and lower than the other pair
            elif (Z[i, j] > Z[i+1, j+1] and Z[i, j] > Z[i-1, j-1] and Z[i, j] < Z[i+1, j-1] and Z[i, j] < Z[i-1, j+1]) \
                or (Z[i, j] > Z[i+1, j-1] and Z[i, j] > Z[i-1, j+1] and Z[i, j] < Z[i+1, j+1] and Z[i, j] < Z[i-1, j-1]):
                result[i-1, j-1] = 1
            elif (Z[i, j] > Z[i-1, j-1] and Z[i, j] < Z[i-1, j+1] and Z[i, j] < Z[i+1, j-1] and Z[i, j] < Z[i+1, j+1]) \
                or (Z[i, j] > Z[i-1, j+1] and Z[i, j] < Z[i-1, j-1] and Z[i, j] < Z[i+1, j-1] and Z[i, j] < Z[i+1, j+1]) \
                or (Z[i, j] > Z[i+1, j-1] and Z[i, j] < Z[i+1, j+1] and Z[i, j] < Z[i-1, j-1] and Z[i, j] < Z[i-1, j+1]) \
                or (Z[i, j] > Z[i+1, j+1] and Z[i, j] < Z[i+1, j-1] and Z[i, j] < Z[i-1, j-1] and Z[i, j] < Z[i-1, j+1]):
                result[i-1, j-1] = 1
    result = np.vectorize(lambda a: a == 1)(result)
    return result


if __name__ == "__main__":
    property = "spring_valley"
    pycharm_path = f'../../../Demos/Data/{property}.txt'
    path_from_root = f'Demos/Data/{property}.txt'
    Z = np.loadtxt(pycharm_path if os.path.exists(pycharm_path) else path_from_root)

    r = shape_classification_method(Z)

    fig = go.Figure(data=[go.Surface(z=Z, colorscale='Viridis', showscale=True)])

    fig.update_traces(contours_z=dict(show=True, usecolormap=True,
                                      highlightcolor="limegreen", project_z=True))

    fig.add_scatter3d(x=np.where(r == True)[1],
                      y=np.where(r == True)[0],
                      z=Z[r == True] + 3,
                      mode='markers',
                      marker=dict(
                          size=2,
                          color='red',
                          opacity=.8
                      ))

    fig.update_layout(title='Spring Valley Farm 3D Plot', autosize=True,
                      margin=dict(l=65, r=50, b=65, t=90))

    fig.show()

