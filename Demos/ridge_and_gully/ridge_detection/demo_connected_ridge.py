"""
author: Yasar
"""
import sys, os
import numpy as np
sys.path.insert(1, os.getcwd())
from Backend.ridge_and_gully.ridge_road_network import get_ridge_network, get_pysheds_gullies, get_gullies
import matplotlib.pyplot as plt

if __name__ == "__main__":
    property = "spring_valley"
    pycharm_path = f'../../../Demos/Data/{property}.txt'
    path_from_root = f'Demos/Data/{property}.txt'
    DEM = np.loadtxt(pycharm_path if os.path.exists(pycharm_path) else path_from_root)

    """
    Specify which gully connection is preferred: get_pysheds_gullies, get_gullies
    """

    # r = get_ridge_network(DEM, get_gullies, 1)
    g = get_gullies(DEM)[2]
    g1 = get_gullies(DEM)[0]
    plt.imshow(DEM)
    xg, yg = np.where(g == 1)
    xg1, yg1 = np.where(g1 == 1)
    # xr, yr = np.where(r == 1)

    # plt.scatter(yr, xr, c='red')
    plt.scatter(yg, xg, c='blue')
    plt.scatter(yg1, xg1, c='green')

    plt.show()

        



    


