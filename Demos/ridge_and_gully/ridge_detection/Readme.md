## Ridge Detection

### Introduction
The Ridge detection algorithm is designed to detect ridges from terrain maps. 
In order to achieve this goal. We need to specify three important problems.
* What is a ridge?
* Why ridge detection is useful in our project?
* How are we going to implement a method?

### What is a ridge?
According to Wikipedia, A ridge or a mountain ridge is a geographical feature consisting of a chain of 
mountains or hills that form a continuous elevated crest for some distance. So basically, if we recognize a 
height terrain map as an image, ridge is that pixels that have great descent gradient with its neighbour pixels.

### Why ridge detection is useful in our project?
In our project, if we want to design road plans in the farm, we need to know where ridges are, so that we can avoid 
dangerous parts in our map. So ridge detection is an important part of road plan.

### How are we going to implement a method?
Shinji Koka et al. 2011 implemented a method named "Ridge detection with the Steepest Ascent Method". I'll explain this method 
briefly there. In this essay, Shinji Koka proposed a new algorithm which is based on steepest ascent lines obtained by selecting the 
maximum inclined direction from eight neighbours, then we can extract ridge lines by their steepest ascent lines.

### Features
* *Input*
  * Grid: Array (mxn)
* *Output*
  * A 0-1 mxn matrix where 1 represent ridge
* *Sample result*
![avatar](ridges.png)
### References
1. Wikipedia (2020), *Ridge*, 11 July 2020, Available at: https://en.wikipedia.org/wiki/Ridge.
2. Koka, Shinji & Anada, Koichi & Nomaki, Kenshi & Sugita, Kimio & Tsuchida, Kensei & Yaku, Takeo. (2011). Ridge Detection with the Steepest Ascent Method. Procedia CS. 4. 216-221. 10.1016/j.procs.2011.04.023. 