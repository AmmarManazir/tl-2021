import numpy as np
import plotly.graph_objects as go
from Demos.ridge_and_gully.ridge_detection.ridge_detection import steepest_descent_method, steepest_ascent_method

import os


def load_dem_data(pycharm_path, path_from_root):
    return np.loadtxt(pycharm_path if os.path.exists(pycharm_path) else path_from_root)


def visualize_DEM(Z, acc):
    # Visualization of the DEM data based on the accumulation data color
    fig = go.Figure(data=[go.Surface(z=Z, surfacecolor=acc/np.max(acc) * np.max(Z), showscale=True)])

    fig.update_traces(contours_z=dict(show=True, usecolormap=True,
                                      highlightcolor="limegreen", project_z=True))

    fig.update_layout(title='Spring Valley Farm 3D Plot', autosize=True,
                      margin=dict(l=65, r=50, b=65, t=90))

    fig.show()


def visualize_accumulation(acc):
    # Visualization of the accumulation data
    fig = go.Figure(data=[go.Surface(z=acc, showscale=True)])

    fig.update_traces(contours_z=dict(show=True, usecolormap=True,
                                      highlightcolor="limegreen", project_z=True))

    fig.update_layout(title='Spring Valley Farm 3D Plot',
                      margin=dict(l=65, r=50, b=65, t=90))

    fig.show()


if __name__ == "__main__":
    pycharm_path = '../Data/spring_valley.txt'
    path_from_root = 'Backend/Data/spring_valley.txt'
    Z = load_dem_data(pycharm_path, path_from_root)

    _, r = steepest_ascent_method(Z, 30)
    _, g = steepest_descent_method(Z, 30)
    visualize_DEM(Z, r)
    visualize_DEM(Z, g)
    visualize_accumulation(r)
    visualize_accumulation(g)