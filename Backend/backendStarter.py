# Backend/backendStarter.py

# This is the starting point for all of our Backend functionality
# and serves as the link between the frontend and Backend.

import sys, os

sys.path.insert(1, os.getcwd())
sys.path.append('.')
import numpy as np
# import psycopg2

from Backend.trench_plan.step_terrain import trench_plan as trenches_step
from Backend.trench_plan.evenly_spaced_trenches import trench_plan as trenches_even
from Backend.ridge_and_gully.pysheds_accumulation import closed_gullies, closed_ridges
from Backend.ridge_and_gully.peak_connections import peak_connections
from Backend.ridge_and_gully.ridge_road_network import get_ridge_network, get_pysheds_gullies, get_gullies

from Backend.queries.querying import generateDEM as generate_DEM_preprocessed
from Backend.queries.Sandpit_NSW.query_interface_gp import get_cont_from_point_gp, RGB_aerial_gp, \
    get_cont_from_two_points_gp, get_cad_mask, RGB_aerial_two_points_gp

results = dict()

# This reruns the specified function using the provided threshold
# Input: Dict with the layer name associated with the new threshold. e.g. {"ridge":99}
# Returns: Eeeeeeverything in 'results' dict
def update_layer(formDict):
    try:
        # TODO: Since the form always has just one element, could use next(formDict.values()) to remove code duplication
        DEM = np.asarray(results['DEM'])
        if "ridges_pysheds" in formDict:
            return closed_ridges(DEM, float(formDict["ridges_pysheds"])).tolist()
        if "gullies_pysheds" in formDict:
            return closed_gullies(DEM, float(formDict["gullies_pysheds"])).tolist()
        if "trenches_even" in formDict:
            return trenches_even(DEM, int(formDict["trenches_even"])).tolist()
        if "trenches_step" in formDict:
            return trenches_step(DEM, int(formDict["trenches_step"])).tolist()
        if "aerial" in formDict:
            if type(results["coords"][0]) == list:
                print(f"Getting aerial imagery from two points")
                return RGB_aerial_two_points_gp(results["shape"], results["coords"][0], results["coords"][1]).tolist()
            else:
                print(f"Getting aerial imagery from one point")
                return RGB_aerial_gp(results["shape"], results["coords"][0], results["coords"][1]).tolist()
        if "cadastre" in formDict:
            if type(results["coords"][0]) == list:
                results['statement'] = "Please specify a single coordinate to retrieve the cadastre"
            else:
                return get_cad_mask(results["shape"], results["coords"][0], results["coords"][1]).tolist()
    except Exception as e:
        print(f"Error updating layer from request {formDict} because {e}")
    return results


# This is the main function that starts the Backend.
# Input: coordinates of property as duple
# Returns: Eeeeeeverything in 'results' dict
def backend_main(coord):
    results['statement'] = ""
    results['DEM'] = None

    # Step 0: Verify that input is syntactically correct
    # e.g. "-34.737893, 150.530024" or "-34.737893, 150.530024; -34.737893, 150.530024"
    coords = parse_coord(coord)
    results["coords"] = coords

    if not coords:
        print("oof")
        results['statement'] = "Please make sure the input is syntactically correct (Eg: -35.28878, 149.01080)"
        return results

    print(f"Started Backend for coords: {coord}")

    # Step 1: establish database connection (Development)
    try:
        # connection = psycopg2.connect(user="me",
        #                               password="me",
        #                               host="localhost",
        #                               port="5432",
        #                               database="act")

        # Step 1: establish database connection (Production)
        # DATABASE_URL = os.environ['DATABASE_URL']
        # connection = psycopg2.connect(DATABASE_URL, sslmode='require')

        results['statement'] = "DEM for " + coord

        # Ste2: Get DEM
        cell_size = 10
        results['cell_size'] = cell_size
        if type(coords[0]) == list:
            print(f"Getting dem from two points")
            results['DEM'] = get_cont_from_two_points_gp(coords[0], coords[1], cell_size).tolist()

        else:
            # Quick load for testing
            results['DEM'] = np.loadtxt('Demos/Data/spring_valley.txt').tolist()

            # Get DEM (preprocessed - database setup required)
            # results['DEM'] = generate_DEM_preprocessed(coords, connection)

            # Get DEM (webgis - no setup required)
            # if not results['DEM']:
            #     print(f"Getting dem from one point with get_cont_from_point_gp")
            #     DEM = get_cont_from_point_gp(coords[0], coords[1], cell_size)
            #     results['DEM'] = DEM.tolist()
        print(f"Got DEM of size: {len(results['DEM'])}x{len(results['DEM'][0])}")

    except Exception as e:
        print(f"Could not generate dem because {e}")
        results[
            'statement'] = "Could not generate Digital Elevation Model for " + coord + ". Please try a different coordinate"
        results['DEM'] = None

    # Step 3: Initialise boolean maps
    layers = {"trenches_step": 5,
              "trenches_even": 10,
              "gullies_pysheds": 99,
              "ridges_pysheds": 98,
              "cadastre": 10,
              "aerial": 10
              }
    if results['DEM']:
        try:
            for layer in layers:
                results[layer] = dict()
                results[layer]["starting_value"] = layers[layer]

            results["shape"] = [len(results['DEM']), len(results['DEM'][0])]

            # Precalculate just the layers that load really fast (No API calls please!)
            DEM = np.asarray(results['DEM'])
            results['trenches_even']['array'] = trenches_even(DEM, layers['trenches_even']).tolist()
            results['ridges_pysheds']['array'] = closed_ridges(np.asarray(results['DEM']), layers["ridges_pysheds"]).tolist()
            results['gullies_pysheds']['array'] = closed_gullies(np.asarray(results['DEM']), layers["gullies_pysheds"]).tolist()

        except Exception as e:
            print("Could not initialise layers because", e)
    return results


def parse_coord(coord):
    """ Example inputs:
            Single coord : '-34.737893, 150.530024'
            Corner coords: '-34.737893, 150.530024; -34.737893, 150.530024'
        Example returns:
            Single coord : ['-34.737893', '150.530024']
            Corner coords: [[-34.737893, 150.530024],[-34.737893, 150.530024]]"""
    if ';' in coord:
        try:
            coords = [c.split(',') for c in coord.split(';')]
            coords = [[float(c) for c in cs] for cs in coords]
        except:
            coords = None
    else:
        try:
            coords = coord.split(',')
            float(coords[0]), float(coords[1])
        except:
            coords = None
    return coords


def test(name):
    return "Hello from the Backend, " + name + "! :D"