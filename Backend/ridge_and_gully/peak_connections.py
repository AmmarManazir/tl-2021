"""
author: Yasar
"""

import numpy as np
from scipy import interpolate
from skimage.feature import peak_local_max
from pathfinding.core.diagonal_movement import DiagonalMovement
from pathfinding.core.grid import Grid
from pathfinding.finder.bi_a_star import BiAStarFinder
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import minimum_spanning_tree


def peak_connections(Z, peak_size=4, downscale=1):
    """Connects the high points and avoids steep terrain"""
    coordinates = peak_local_max(Z, peak_size)
    print(f"Found {len(coordinates)} peaks")

    # # Downscale grid to speed things up (Need to figure out how to upscale at the end)
    # new_shape = (int(Z.shape[0] / downscale) + 1, int(Z.shape[1] / downscale) + 1)
    # coordinates = new_pos(coordinates, Z.shape, (new_shape[0] - 1, new_shape[1] - 1))
    # Z = resize_grayscale(Z, new_shape)
    # Z = np.nan_to_num(Z)
    # print(f"Shape after downscaling: {Z.shape}")

    # Calculate the gradients
    x, y = np.gradient(Z)
    g = ((x ** 2 + y ** 2)) * 10 + 10
    matrix = g.T.tolist()

    # Generate paths
    paths = []
    span_dists = np.zeros((coordinates.shape[0], coordinates.shape[0]))
    span_paths = dict()
    for i in range(coordinates.shape[0]):
        other_peaks = coordinates.copy()
        grid = Grid(matrix=matrix)
        finder = BiAStarFinder(diagonal_movement=DiagonalMovement.always)
        diff = other_peaks - coordinates[i]
        dists = diff[:, 0] ** 2 + diff[:, 1] ** 2
        goal_coords = dists.argsort()[1:int(coordinates.shape[0] / 2)]
        print(f"Finding path for coords: {goal_coords}")
        for j in goal_coords:
            if span_dists[(i, j)]:
                continue
            goal = coordinates[j]
            path = np.array(get_path(coordinates[i], goal, finder, grid))
            paths.append(path)
            grid = Grid(matrix=matrix)
            finder = BiAStarFinder(diagonal_movement=DiagonalMovement.always)
            cost = np.sum(g[path[:, 0], path[:, 1]])
            span_dists[i, j] = cost
            span_paths[(i, j)] = path
            span_dists[j, i] = cost
            span_paths[(j, i)] = path
    X = csr_matrix(span_dists)

    # Find the minimum spanning tree
    Tcsr = minimum_spanning_tree(X).toarray().astype(int)
    idx, idy = np.where(Tcsr != 0)
    coords = np.column_stack((idx, idy)).tolist()

    roads = np.zeros(Z.shape, dtype=bool)
    for coord in coords:
        i, j = coord
        path = span_paths[(i, j)]
        roads[tuple(path.T)] = True
    return roads


def resize_grayscale(img, shape):
    current_shape = img.shape
    """
    shape is basically (height, width)
    """
    width = current_shape[1]
    height = current_shape[0]
    width_new = shape[1]
    height_new = shape[0]
    axis_coordinates = lambda size: np.linspace(0, 1, size)
    f = interpolate.interp2d(axis_coordinates(width), axis_coordinates(height), img)
    resized_img = f(axis_coordinates(width_new), axis_coordinates(height_new))
    return resized_img


def get_path(s, g, finder, grid):
    start = grid.node(s[0], s[1])
    end = grid.node(g[0], g[1])
    path, runs = finder.find_path(start, end, grid)
    return path


def new_pos(coordinates, current_shape, new_shape):
    current_width, current_height = current_shape
    new_width, new_height = new_shape

    x_coordinates, y_coordinates = coordinates[:, 0], coordinates[:, 1]

    new_x = (x_coordinates / current_width) * new_width
    new_y = (y_coordinates / current_height) * new_height

    return np.column_stack((new_x.astype(np.int64), new_y.astype(np.int64)))




