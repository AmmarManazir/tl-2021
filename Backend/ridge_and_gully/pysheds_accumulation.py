import numpy as np
from affine import Affine
from pyproj import Proj
from pysheds.grid import Grid


def gullies(Z, threshold):
    Z = np.array(Z, dtype=float)
    acc = water_accumulation(Z)
    acc_threshold = np.percentile(acc, threshold)
    gullies = np.vectorize(lambda a: a > acc_threshold)(acc)
    return gullies


def ridges(Z, threshold):
    Z = np.array(Z, dtype=float)
    return gullies(-Z, threshold)


# Takes a 2D array of height values and returns a 2D array of probabilities
def water_accumulation(Z):
    grid = dem_to_grid(Z)

    # Conditioning terrain for water flow
    grid.fill_depressions(data='dem', out_name='flooded_dem')
    grid.resolve_flats(data='flooded_dem', out_name='inflated_dem')

    grid.flowdir(data='inflated_dem', out_name='dir')
    grid.accumulation(data='dir', out_name='acc')
    acc = grid.view('acc')
    return np.array(acc)


# Convert the DEM (list of height values) to a Grid (data structure used by Pysheds)
def dem_to_grid(Z):
    grid = Grid()
    # crs is the coordinate reference system: https://epsg.io/28355
    # Affine identity means no extra transformations: https://www.e-education.psu.edu/natureofgeoinfo/c2_p18.html
    grid.add_gridded_data(data=Z, data_name='dem', affine=Affine.identity(), shape=Z.shape,
                          crs=Proj('epsg:4326'))
    return grid


def get_neighbour_coordinate(acc, point):
    # Get 8 neighbour cells, ignore those out of boundary
    y, x = point[0], point[1]
    neighbour_coordinate = []

    for i in range(y - 1, y + 2):
        for j in range(x - 1, x + 2):
            if i == y and j == x:
                continue
            if 0 <= i < acc.shape[0] and 0 <= j < acc.shape[1]:
                neighbour_coordinate.append([i, j])
    neighbour_coordinate = np.array(neighbour_coordinate)

    return neighbour_coordinate


def get_adjacent_cells(acc, point, neighbour_coordinate, ridges_coordinate, gullies_coordinate):
    # Get adjacent cells
    y, x = point[0], point[1]
    adjacent_higher_rgs, adjacent_lower_rgs, adjacent_higher_cells, adjacent_lower_cells = [], [], [], []
    for p in neighbour_coordinate:
        overlap_r = np.array([])
        overlap_g = np.array([])
        if len(ridges_coordinate > 0):
            overlap_r = np.where((ridges_coordinate == p).all(1))[0]
        if len(gullies_coordinate > 0):
            overlap_g = np.where((gullies_coordinate == p).all(1))[0]
        # Get adjacent ridges and creeks that are higher or equal to the cell
        if (len(overlap_r) != 0 or len(overlap_g) != 0) and acc[p[0], p[1]] >= acc[y, x]:
            adjacent_higher_rgs.append(p)
        # Get adjacent ridges and creeks that are lower or equal to the cell
        if (len(overlap_r) != 0 or len(overlap_g) != 0) and acc[p[0], p[1]] <= acc[y, x]:
            adjacent_lower_rgs.append(p)
        # Get adjacent cells that are higher or equal to the cell (except ridges and creeks)
        if not (len(overlap_r) != 0 or len(overlap_g) != 0) and acc[p[0], p[1]] >= acc[y, x]:
            adjacent_higher_cells.append(p)
        # Get adjacent cells that are lower or equal to the cell (except ridges and creeks)
        if not (len(overlap_r) != 0 or len(overlap_g) != 0) and acc[p[0], p[1]] <= acc[y, x]:
            adjacent_lower_cells.append(p)
    adjacent_higher_rgs = np.array(adjacent_higher_rgs)
    adjacent_higher_cells = np.array(adjacent_higher_cells)
    adjacent_lower_rgs = np.array(adjacent_lower_rgs)
    adjacent_lower_cells = np.array(adjacent_lower_cells)

    return (adjacent_higher_rgs, adjacent_higher_cells, adjacent_lower_rgs, adjacent_lower_cells)


def get_new_terrain(acc, neighbour_coordinate, adjacent_terrains, r, g, new_terrain_list, get_ridge=True,
                    get_gully=False):
    (adjacent_higher_rgs, adjacent_higher_cells, adjacent_lower_rgs, adjacent_lower_cells) = adjacent_terrains

    # This doesn't work because the threshold ensures any higher cells were already previously chosen
    # Need a different way to connect cells with high accumulations
    if len(adjacent_higher_rgs) == 0 and len(adjacent_higher_cells) != 0:
        if get_ridge:
            index = np.where(acc[neighbour_coordinate[:, 0], neighbour_coordinate[:, 1]] == np.max(
                acc[adjacent_higher_cells[:, 0], adjacent_higher_cells[:, 1]]))
            i, j = neighbour_coordinate[index, 0][0][0], neighbour_coordinate[index, 1][0][0]
            new_terrain_list.append([i, j])
            r[i, j] = True
        if get_gully:
            # This may be redundant because most (all?) gullies exit the property
            index = np.where(acc[neighbour_coordinate[:, 0], neighbour_coordinate[:, 1]] == np.max(
                acc[adjacent_higher_cells[:, 0], adjacent_higher_cells[:, 1]]))
            i, j = neighbour_coordinate[index, 0][0][0], neighbour_coordinate[index, 1][0][0]
            new_terrain_list.append([i, j])
            g[i, j] = True

    # If there are no adjacent ridge/gully cells with a lower accumulation
    if len(adjacent_lower_rgs) == 0 and len(adjacent_lower_cells) != 0:
        if get_ridge:
            # find the least low cell that is still lower and make it a ridge
            index = np.where(acc[neighbour_coordinate[:, 0], neighbour_coordinate[:, 1]] == np.max(
                acc[adjacent_lower_cells[:, 0], adjacent_lower_cells[:, 1]]))
            i, j = neighbour_coordinate[index, 0][0][0], neighbour_coordinate[index, 1][0][0]
            new_terrain_list.append([i, j])
            r[i, j] = True
        if get_gully:
            # find the highest accumulation that is still lower
            index = np.where(acc[neighbour_coordinate[:, 0], neighbour_coordinate[:, 1]] == np.max(
                acc[adjacent_lower_cells[:, 0], adjacent_lower_cells[:, 1]]))
            i, j = neighbour_coordinate[index, 0][0][0], neighbour_coordinate[index, 1][0][0]
            new_terrain_list.append([i, j])
            g[i, j] = True


def closure(acc_r, acc_g, r, g):
    # Initialization
    ridges_coordinate = np.array([[y_r, x_r] for y_r, x_r in zip(np.where(r == 1)[0], np.where(r == 1)[1])])
    gullies_coordinate = np.array([[y_g, x_g] for y_g, x_g in zip(np.where(g == 1)[0], np.where(g == 1)[1])])

    ridges_list = []
    for ridge in ridges_coordinate:
        neighbour_coordinate = get_neighbour_coordinate(acc_r, ridge)
        (adjacent_higher_rgs, adjacent_higher_cells, adjacent_lower_rgs, adjacent_lower_cells) = get_adjacent_cells(
            acc_r,
            ridge, neighbour_coordinate,
            ridges_coordinate,
            gullies_coordinate)
        get_new_terrain(acc_r, neighbour_coordinate,
                        (adjacent_higher_rgs, adjacent_higher_cells, adjacent_lower_rgs, adjacent_lower_cells),
                        r, g, ridges_list,
                        get_ridge=True,
                        get_gully=False)
    ridges_list = np.array(ridges_list)

    gullies_list = []
    for gully in gullies_coordinate:
        neighbour_coordinate = get_neighbour_coordinate(acc_g, gully)
        (adjacent_higher_rgs, adjacent_higher_cells, adjacent_lower_rgs, adjacent_lower_cells) = get_adjacent_cells(
            acc_g,
            gully, neighbour_coordinate,
            ridges_coordinate,
            gullies_coordinate)

        get_new_terrain(acc_g, neighbour_coordinate,
                        (adjacent_higher_rgs, adjacent_higher_cells, adjacent_lower_rgs, adjacent_lower_cells),
                        r, g, gullies_list,
                        get_ridge=False,
                        get_gully=True)
    gullies_list = np.array(gullies_list)

    # Repeat until converge
    while True:
        ridges_coordinate = np.array([[y_r, x_r] for y_r, x_r in zip(np.where(r == 1)[0], np.where(r == 1)[1])])
        gullies_coordinate = np.array([[y_g, x_g] for y_g, x_g in zip(np.where(g == 1)[0], np.where(g == 1)[1])])

        new_ridges_list = []
        for ridge in ridges_list:
            neighbour_coordinate = get_neighbour_coordinate(acc_r, ridge)
            (adjacent_higher_rgs, adjacent_higher_cells, adjacent_lower_rgs, adjacent_lower_cells) = get_adjacent_cells(
                acc_r,
                ridge, neighbour_coordinate,
                ridges_coordinate,
                gullies_coordinate)
            get_new_terrain(acc_r, neighbour_coordinate,
                            (adjacent_higher_rgs, adjacent_higher_cells, adjacent_lower_rgs, adjacent_lower_cells),
                            r, g, new_ridges_list,
                            get_ridge=True,
                            get_gully=False)

        new_gullies_list = []
        for gully in gullies_list:
            neighbour_coordinate = get_neighbour_coordinate(acc_g, gully)
            (adjacent_higher_rgs, adjacent_higher_cells, adjacent_lower_rgs, adjacent_lower_cells) = get_adjacent_cells(
                acc_g,
                gully, neighbour_coordinate,
                ridges_coordinate,
                gullies_coordinate)

            get_new_terrain(acc_g, neighbour_coordinate,
                            (adjacent_higher_rgs, adjacent_higher_cells, adjacent_lower_rgs, adjacent_lower_cells),
                            r, g, new_gullies_list,
                            get_ridge=False,
                            get_gully=True)

        if len(new_gullies_list) == 0 and len(new_ridges_list) == 0:
            break
        ridges_list = new_ridges_list
        gullies_list = new_gullies_list

def closed_gullies(Z, threshold):
    Z = np.array(Z, dtype=float)
    g = gullies(Z, threshold)
    r = np.zeros(Z.shape, dtype=bool)
    acc = water_accumulation(Z)
    closure(acc, acc, r, g)
    return g

def closed_ridges(Z, threshold):
    Z = np.array(Z, dtype=float)
    g = np.zeros(Z.shape, dtype=bool)
    r = ridges(Z, threshold)
    acc = water_accumulation(-Z)
    closure(acc, acc, r, g)
    return r

