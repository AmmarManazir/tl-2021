"""
author: Yasar
"""
import sys, os
import numpy as np
from skimage.morphology import square, diamond, closing, dilation, skeletonize, white_tophat, black_tophat, label
sys.path.insert(1, os.getcwd())
from Backend.ridge_and_gully.pysheds_accumulation import gullies as pysheds_gullies, ridges
from scipy.ndimage import gaussian_filter
from skimage.graph import route_through_array
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import minimum_spanning_tree


def is_extreme_point(point, ridge_map, intensity):
    x, y = point
    sum = ridge_map[x - 1, y + 1] + ridge_map[x, y + 1] +  ridge_map[x + 1, y + 1] + ridge_map[x - 1, y] + ridge_map[x + 1, y] + ridge_map[x - 1, y - 1] + ridge_map[x, y -1 ] + ridge_map[x + 1, y - 1]
    if sum == intensity:
        return True
    return False

def get_ridges(DEM):
    Z = gaussian_filter(DEM ** 2, 5)
    ridges = white_tophat(Z, diamond(3))
    ridges = ridges + white_tophat(Z, square(3))
    ridges[ridges > 0] = 1
    ridges_dilated = closing(ridges, diamond(8))
    ridges_skeleton = skeletonize(ridges_dilated)
    ridges_x, ridges_y = np.where(ridges_skeleton != 0)
    ridges_coords = np.column_stack((ridges_x, ridges_y))
    ridges_labels = label(dilation(ridges_skeleton, square(2)), connectivity=2)
    return ridges_skeleton , ridges_coords, ridges_labels

def get_pysheds_gullies(DEM, threshold = 98):
    gullies = pysheds_gullies(DEM, threshold)
    gullies_skeleton = dilation(gullies, square(3))
    gullies_x, gullies_y = np.where(gullies_skeleton != 0)
    gullies_coords = np.column_stack((gullies_x, gullies_y))
    gullies_labels = label(dilation(gullies_skeleton, square(2)), connectivity=2)
    return gullies_skeleton.astype(np.uint8), gullies_coords, gullies_labels

def get_gullies(DEM, threshold = 98):
    Z = gaussian_filter(DEM ** 2, 6)
    gullies = black_tophat(Z, diamond(3))
    gullies = gullies + black_tophat(Z, square(3))
    gullie_scores = gullies.copy()
    gullies[gullies > 0] = 1
    gullies_dilated = closing(gullies, diamond(8))
    gullies_skeleton = skeletonize(gullies_dilated)
    gullies_skeleton = dilation(gullies_skeleton, square(3))
    gullies_x, gullies_y = np.where(gullies_skeleton != 0)
    gullies_coords = np.column_stack((gullies_x, gullies_y))
    gullies_labels = label(gullies_skeleton, connectivity=2)
    return skeletonize(gullies_skeleton).astype(np.uint8), gullies_coords, gullies_labels


def get_ridge_junctions(ridges_labels, gullies_skeleton):
    ridge_junctions = []
    for ridge_label in np.unique(ridges_labels):
        ridge_map = ridges_labels.copy()
        ridge_map[ridge_map != ridge_label] = 0
        ridge_map[ridge_map > 0] = 1
        ridge_x, ridge_y = np.where(ridge_map == 1)
        ridge_coords = np.column_stack((ridge_x, ridge_y))
        for ridge_coord in ridge_coords:
            if not(ridge_coord[0] >= ridge_map.shape[0] - 1) and not(ridge_coord[1] >= ridge_map.shape[1] - 1):
                if is_extreme_point(ridge_coord, ridge_map, 3) or is_extreme_point(ridge_coord, ridge_map, 1):
                    ridge_junctions.append(ridge_coord)

    j = np.array(ridge_junctions)
    Filter = np.zeros_like(ridges_labels)
    Filter[j[:,0], j[:,1]] = 1
    rx, ry = np.where(Filter - gullies_skeleton == 1)
    ridge_junctions = np.column_stack((rx, ry))
    return ridge_junctions


def connect_ridges(ridges_skeleton, ridges_labels, coordinates, costs):
    
    span_dists = np.zeros((coordinates.shape[0], coordinates.shape[0]))
    span_paths = dict()
    checked = span_dists.copy()
    delete_from_tree = span_dists.copy()
    for i in range(len(coordinates)):
        start = coordinates[i]
        for j in range(len(coordinates)):
            goal = coordinates[j]
            if i == j or checked[i, j] == 1 or checked[j, i] == 1:
                continue
            if ridges_labels[start[0], start[1]] == ridges_labels[goal[0], goal[1]]:
                delete_from_tree[i, j] = 1
                delete_from_tree[j, i] = 1
                continue
            try:
                path, cost = route_through_array(costs, start, goal, fully_connected=True)
                path_exists = True
                
            except:
                path_exists = False
            if path_exists:
                span_dists[i, j] = cost
                span_paths[(i, j)] = np.array(path)
                span_dists[j, i] = cost
                span_paths[(j, i)] = np.array(path)
                checked[i, j] = 1
                checked[j, i] = 1
               

    ridges_and_roads = np.zeros_like(ridges_labels)
    X = csr_matrix(span_dists)
    Tcsr = minimum_spanning_tree(X).toarray().astype(int)
    idx, idy = np.where(Tcsr != 0)
    coords = np.column_stack((idx, idy)).tolist()
    for coord in coords:
        i, j = coord
        if delete_from_tree[i, j] == 1 or delete_from_tree[i, j] == 1:
            continue
        path = span_paths[(i, j)]
        ridges_and_roads[path[:,0], path[:,1]] = 1

    ridges_x, ridges_y = np.where(ridges_skeleton != 0)
    ridges_and_roads[ridges_x, ridges_y] = 1
    ridges_and_roads = skeletonize(closing(ridges_and_roads, diamond(3))).astype(np.uint8)

    return ridges_and_roads

def get_ridge_network(DEM, gully_method, threshold):
    ridges_skeleton, ridges_coords, ridges_labels = get_ridges(DEM)
    gullies_skeleton, gullies_coords, gullies_labels = gully_method(DEM, threshold)
    ridges_junctions = get_ridge_junctions(ridges_labels, gullies_skeleton)
    DEM_copy = np.nan_to_num(DEM)
    x, y = np.gradient(DEM_copy)
    costs = ((x**2 + y ** 2) ) * 10 + 10 
    costs[gullies_coords[:,0], gullies_coords[:,1]] = -1
    ridges_and_roads = connect_ridges(ridges_skeleton, ridges_labels, ridges_junctions, costs)
    Filter = ridges_and_roads - gullies_skeleton
    xr, yr = np.where(Filter == 1)
    ridges_and_roads = np.zeros_like(gullies_skeleton)
    ridges_and_roads[xr, yr] = 1
    return ridges_and_roads

    


