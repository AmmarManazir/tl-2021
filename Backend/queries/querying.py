import psycopg2
import numpy as np
import pickle

TABLE_NAME = "dems"

def generateDEM(coords, connection):
    cursor = connection.cursor()
    cursor.execute(f"""select dem from {TABLE_NAME}
                    where ST_Contains({TABLE_NAME}.boundary,
                    st_transform(ST_SetSRID(ST_Point({coords[1]}, {coords[0]}), 4326), 28355));
                   """)
    fetched = cursor.fetchone()
    if not fetched:
        return None
    else:
        return pickle.loads(fetched[0]).tolist()

# TODO: Handle properties that are not in the database for the rest of these functions too
def generateID(coords, connection):
    cursor = connection.cursor()
    cursor.execute(f"""select id from {TABLE_NAME}
                    where ST_Contains({TABLE_NAME}.boundary,
                    st_transform(ST_SetSRID(ST_Point({coords[1]}, {coords[0]}), 4326), 28355));
                   """)
    id_ = cursor.fetchone()
    return id_[0]


def generateBoundary(coords, connection):
    cursor = connection.cursor()
    cursor.execute(f"""select ST_X((dp).geom) AS x, ST_Y((dp).geom) AS y 
                    FROM (select ST_Dumppoints(boundary) as dp from {TABLE_NAME}
                    where ST_Contains({TABLE_NAME}.boundary,
                    st_transform(ST_SetSRID(ST_Point({coords[1]}, {coords[0]}), 4326), 28355))) as points;
                       """)
    boundary = cursor.fetchall()
    return boundary


def generateAddress(coords, connection):
    cursor = connection.cursor()
    cursor.execute(f"""select address from {TABLE_NAME}
                    where ST_Contains({TABLE_NAME}.boundary,
                    st_transform(ST_SetSRID(ST_Point({coords[1]}, {coords[0]}), 4326), 28355));
                   """)
    address = cursor.fetchone()
    return address[0]


def generateExtent(coords, connection):
    cursor = connection.cursor()
    cursor.execute(f"""select ST_Extent(boundary) from {TABLE_NAME}
                        where ST_Contains({TABLE_NAME}.boundary,
                        st_transform(ST_SetSRID(ST_Point({coords[1]}, {coords[0]}), 4326), 28355));
                       """)
    extent = cursor.fetchone()
    e = extent[0]
    e = e[e.find('(') + 1:e.find(')')].replace(",", " ").split()
    return np.array(e, dtype=float)





def coord_string_to_tuple(coord):
    return [float(c.strip()) for c in coord.split(',')]
