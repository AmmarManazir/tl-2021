import sys, os
sys.path.insert(1, os.getcwd())
from io import BytesIO
from shapely.wkb import loads
from shapely.geometry import Point, LineString
import geopandas as gp
import pandas as pd
import shapely.geometry
import numpy as np
from rasterio import features
from requests import Request, get
import matplotlib.pyplot as plt
from pyproj import Proj, Transformer
from pandas import json_normalize
from PIL import Image
from Backend.queries.interpolate import generate_grid



def generate_cad_points_gp(lat, long, isACT):
    """
    Generates coordinate box in both NSW (3857) and ACT (28355) Spatial Reference

    :param lat: latitude
    :param long: longatude
    :param isACT: boolean indicating if its within ACT
    :return: bound - coordinate in spatial reference of its State (if isACt = true, ACT spatial reference), other - cooridate in spatial reference of the other state
    """
    if isACT:
        outputEPSG = 28355
        Cadastre_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/basic/MapServer/101/query?geometry="
    else:
        Cadastre_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/Cadastre/MapServer/0/query?geometry="
        outputEPSG = 3857

    box_bound = generate_box_points_gp(lat, long, 0, isACT)
    gs = generate_shape_gp(box_bound, Cadastre_url)
    extent = gs.total_bounds
    bound = gp.GeoSeries([Point(extent[0], extent[1]), Point(extent[2], extent[3])])
    if isACT:
        inEPSG = 28355
        outEPSG = 3857
    else:
        outEPSG = 28355
        inEPSG = 3857
    x, y = transform_proj(extent[0], extent[1], inEPSG, outEPSG)
    p1 = Point(x,y)
    x, y = transform_proj(extent[2], extent[3], inEPSG, outEPSG)
    p2 = Point(x,y)
    other = gp.GeoSeries([p1, p2])
    # gs_line = gp.GeoSeries(LineString(gs.geometry[0].exterior))
    # gd_line = gp.GeoDataFrame(geometry=gs_line)
    return bound, other, gs

def check_if_ACT(lat, long):
    act_border = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/basic/MapServer/0/query?geometry="
    x, y = transform_proj(lat, long, 4326, 28355)
    bound = gp.GeoSeries([Point(x, y), Point(x,y)])
    ds = generate_shape_gp(bound, act_border)
    return len(ds) > 0

def generate_image_gp(bound, feature_url, q_x, q_y):
    """
    Generate an Image

    :param bound:
    :param feature_url:
    :return:
    """
    url = generate_image_gp_URL(bound, feature_url, q_x, q_y)
    ret = get(url)
    print(ret)
    img = Image.open(BytesIO(ret.content))
    return img

def generate_image_gp_URL(bound, feature_url, q_x, q_y):

    """
    Generate the full image query URL

    :param bound: the box coordinates
    :param feature_url: the feature_url of the map/image server
    :return: full url for query image
    """

    url = feature_url + str(bound[0].x)+ "," + str(bound[0].y) + "," + str(bound[1].x) + "," + str(bound[1].y)
    url = url + "&size=" + str(q_x) + "%2C" + str(q_y)
    url = url + "&format=png&transparent=true&f=image"
    return url

def generate_shape_gp(bound, feature_url):
    """
    Generate a ogr dataset from the query return

    :param bound: box coordinate
    :param feature_url: the feature_url of the map serevr
    :return: response dataset of the query
    """
    url = generate_shape_URL(bound, feature_url)
    print(url)
    data = Request('GET', url, headers={'Cache-Control': 'no-cache'}).prepare().url
    gd = gp.read_file(data)
    return gd

def generate_shape_URL(bound, feature_URL):
    """
    Generate the full shape query URL

    :param bound: box coordinate
    :param feature_URL: the feature_url of the map server
    :return: full url for query shape
    """
    url = feature_URL
    url = url + str(bound[0].x)+ "," + str(bound[0].y) + "," + str(bound[1].x) + "," + str(bound[1].y)
    url = url + "&geometryType=esriGeometryEnvelope&f=pjson"
    return url


def generate_rect_bounds_gp(c1, c2, isACT):
    if isACT:
        outputEPSG = 28355
    else:
        outputEPSG = 3857
    inputEPSG = 4326
    x,y = transform_proj(c1[0], c1[1], inputEPSG, outputEPSG)
    p1 = [x,y]
    x,y = transform_proj(c2[0], c2[1], inputEPSG, outputEPSG)
    p2 = [x,y]
    bound = gp.GeoSeries([Point(min(p1[0], p2[0]), min(p1[1],p2[1])), Point(max(p1[0], p2[0]), max(p1[1],p2[1]))])
    return bound

def transform_proj(x, y, inputEPSG, outputEPSG):
    in_str = 'epsg:' + str(inputEPSG)
    out_str = 'epsg:' + str(outputEPSG)
    tf = Transformer.from_crs(in_str, out_str)
    return tf.transform(x, y)

def generate_box_points_gp(lat, long, range, isACT):
    if isACT:
        outputEPSG = 28355
    else:
        outputEPSG = 3857
    inputEPSG = 4326
    x, y = transform_proj(lat, long, inputEPSG, outputEPSG)
    bound = gp.GeoSeries([Point(x-range, y-range), Point(x+range,y+range)])
    return bound

def generate_DEM_gp(prim_bound, contour_url, cell_size):
    #cell_size = 1
    contour = generate_shape_gp(prim_bound, contour_url)
    points = np.empty((0,3))
    extent = prim_bound.total_bounds
    points = np.empty((0, 3))
    for geom in contour.values:
        for pt in geom[1].coords:
            [x,y] = pt
            if ((x < extent[2]) & (x > extent[0]) & (y < extent[3]) & (y > extent[1])):
                points = np.append(points, np.array([[pt[0], pt[1], geom[0]]], dtype=np.float), axis=0)

    dimensions = [(extent[2] - extent[0]), (extent[3] - extent[1])]
    dimensions = [int(round(e)) for e in dimensions]
    width = int(dimensions[0] / cell_size)
    height = int(dimensions[1] / cell_size)
    #slow scipy process
    dem = generate_grid(points, width, height)
    #fast gdal process
    #dem = generate_grid_gdal(contour, width, height)

    return dem

def gen_plt_from_url(plt, data_bound, map_bound, url, color, pt_shp, size):
    ds = generate_shape_gp(data_bound, url)
    points_data = extract_points(ds, size[0], size[1], map_bound)
    for points in points_data:
        if points.ndim > 1:
            plt.plot(points[:, 0], points[:, 1], color = color, rasterized=True)
        else:
            plt.plot(points[0], points[1], pt_shp,  color = color, markersize=15, rasterized=True)
    return plt, points_data

def gen_plt_from_points(points_data, plt, color, pt_shp):
    for points in points_data:
        if points.ndim > 1:
            plt.plot(points[:, 0], points[:, 1], color = color)
        else:
            plt.plot(points[0], points[1], pt_shp,  color = color, markersize=15)
    return plt

def extract_rasterize_points(ds, img_x, img_y, bound):
    points_data = []

    points = np.empty((0, 2))
    min_X = bound[0].x
    max_X = bound[1].x
    min_Y = bound[0].y
    max_Y = bound[1].y
    height = int((max_X - min_X) / img_x)
    width = int((max_Y - min_Y) / img_y)
    print(ds.geometry)
    return

#def convert_polygon_to_polylines(ds):
#    lyr = ds.GetLayer()
#    for feat in lyr:

def extract_points(gs, img_x, img_y, bound):
    points_data = []
    min_X = bound[0].x
    max_X = bound[1].x
    min_Y = bound[0].y
    max_Y = bound[1].y
    dim = [(max_X - min_X), (max_Y - min_Y)]
    ratio_x = img_x / dim[0]
    ratio_y = img_y / dim[1]
    aff_transform = [ratio_x, 0, 0, -ratio_y, - ratio_x * min_X, img_y + min_Y * ratio_y]
    gs = gs.affine_transform(aff_transform)
    for geom in gs.geometry:
        points = np.array(geom)
        points_data.append(points)
    raster = features.geometry_mask(gs.geometry, out_shape=[img_y, img_x], transform=[1.0,0.0,0.0,0.0,1.0,0.0], invert=True)
    return raster, points_data

if __name__ == '__main__':
    contour_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/contours_2015/MapServer/1/query?outFields=contour&geometry="
    lat = -35.055870
    long = 148.990660
    #long = 149.01080 #Spring Valley
    #lat = -35.28878 #Spring Valley
    #p = generate_box_points_gp(lat, long, 100)
    #g = generate_shape_gp(p, contour_url)
    Bld_rural_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/LPIMap/MapServer/22/query?geometry="
    ACT_Cadastre_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/basic/MapServer/101/query?geometry="
    NSW_image_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/public/NSW_Imagery/MapServer/export?bbox="
    ACT_Road_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi2020/basic/MapServer/5/query?geometry="
    ACT_image_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/imagery202001mga/ImageServer/exportImage?bbox="
    ACT_Contour5m_url= "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/contours_2015/MapServer/1/query?outFields=contour&geometry="

    # bound, sec_bound, gs = generate_cad_points_gp(lat, long, False)
    # gs_road = generate_shape_gp(bound, ACT_Road_url)
    #gs_bld = generate_shape_gp(sec_bound, Bld_rural_url)
    #print(extract_points(gs_bld, 1024,1024, sec_bound))
    # img = generate_image_gp(bound, NSW_image_url, 1024, 1824)
    # img2, points = extract_points(gs_road, 1024,1824, bound)
    # img3, points = extract_points(gs, 128, 196, bound)
    # rgb_img = np.array(img)
    # plt.imshow(img3)
    # plt.show()
    #gs_line = gp.GeoSeries(LineString(gs.geometry[0].exterior))
    #gs_contour = generate_shape_gp(bound, ACT_Contour5m_url)
    #dem = generate_DEM_gp(bound, ACT_Contour5m_url,1)


