
import sys, os

sys.path.insert(1, os.getcwd())
from Backend.queries.Sandpit_NSW.query_webgis_gp import *

ACT_Cadastre_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/basic/MapServer/101/query?geometry="
NSW_image_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/public/NSW_Imagery/MapServer/export?format=jpg&bbox="
ACT_Road_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi2020/basic/MapServer/5/query?geometry="
ACT_image_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/imagery202001mga/ImageServer/exportImage?bbox="
NSW_Road_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/LPIMap/MapServer/29/query?geometry="

def RGB_aerial_two_points_gp(shape, c_1, c_2):
    print(shape, c_1, c_2)
    isACT = check_if_ACT(c_1[0], c_1[1])
    # prim_bound, sec_bound, cad_ds = generate_cad_points_gp(f_lat, f_long, isACT)
    bound = generate_rect_bounds_gp(c_1, c_2, isACT)
    if isACT:
        image_url = ACT_image_url
    else:
        image_url = NSW_image_url
    img = generate_image_gp(bound, image_url, shape[1], shape[0])
    rgb_img = np.array(img)
    return rgb_img

def RGB_aerial_gp(shape, f_lat, f_long):
    isACT = check_if_ACT(f_lat, f_long)
    prim_bound, sec_bound, cad_ds = generate_cad_points_gp(f_lat, f_long, isACT)
    if isACT:
        image_url = ACT_image_url
    else:
        image_url = NSW_image_url
    img = generate_image_gp(prim_bound, image_url, shape[1], shape[0])
    rgb_img = np.array(img)
    return rgb_img

def get_cont_from_point_gp(lat, long, cell_size):
    isACT = check_if_ACT(lat, long)
    prim_bound, other_bound, _ = generate_cad_points_gp(lat, long, isACT)
    if isACT:
        contour_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/contours_2015/MapServer/1/query?outFields=contour&geometry="
    else:
        contour_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/LPIMap/MapServer/69/query?outFields=elevation&geometry="
    return generate_DEM_gp(prim_bound, contour_url, cell_size)

def get_cont_from_two_points_gp(c_1, c_2, cell_size):
    isACT = check_if_ACT(c_1[0], c_1[1])
    bound = generate_rect_bounds_gp(c_1, c_2, isACT)
    if isACT:
        contour_url = "https://data.actmapi.act.gov.au/arcgis/rest/services/actmapi/contours_2015/MapServer/1/query?outFields=contour&geometry="
    else:
        contour_url = "https://maps.six.nsw.gov.au/arcgis/rest/services/sixmaps/LPIMap/MapServer/69/query?outFields=elevation&geometry="
    return generate_DEM_gp(bound, contour_url, cell_size)

def get_cad_mask(shape, lat, long):
    isACT = check_if_ACT(lat, long)
    bound, sec_bound, gs = generate_cad_points_gp(lat, long, isACT)
    mask, pts = extract_points(gs, shape[1], shape[0], bound)
    return mask

if __name__ == '__main__':
    # lat = -35.055870
    # long = 148.990660

    # lat = -35.28878       # Spring Valley (in ACT)
    # long = 149.01080
    lat = -36.390927       # Maffra Road (in NSW)
    long = 149.052050

    # lat = -34.737495    # Kangaroo Valley (in NSW)
    # long = 150.530191

    cell_size = 10
    # img2 = RGB_aerial_gp([1024,1024], lat, long)
    # img = get_cad_mask([1024, 1024], lat, long)
    # plt.imshow(img)
    # plt.show()

    DEM = get_cont_from_point_gp(lat, long, cell_size)
    plt.imshow(DEM)
    plt.show()
