"""
    authors: Yasar Adeel Ansari 
"""
import sys, os
sys.path.insert(1, os.getcwd())
import numpy as np
from skimage.measure import find_contours

def extract_contours(Grid):
    Z = Grid.astype(np.uint64)
    dict_Contours = dict()
    for height in range(Z.min(), Z.max()):
        contours_at_height = find_contours(Grid, height)
        dict_Contours[str(height)] = contours_at_height
    return dict_Contours