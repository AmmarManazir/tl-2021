import numpy as np
from Backend.ridge_and_gully.pysheds_accumulation import dem_to_grid
from Backend.trench_plan.Extract_Contours import extract_contours
from Backend.trench_plan.Extract_Trench import extract_trench

def trench_plan(Z, num_trenches=3, step_size=1):
    """returns a boolean array corresponding to all cells chosen as trenches"""
    Z = np.array(Z)
    selected_coords = select_coords(Z, num_trenches, step_size)
    dict_contours = extract_contours(Z)
    trenches = np.zeros(Z.shape,dtype=bool)
    for coord in selected_coords:
        trench = extract_trench(Z, dict_contours, coord).astype(int)
        trenches[tuple(trench.T)] = True
    return trenches

def select_coords(Z, num_trenches=3, step_size=1):
    acc = water_accumulation(Z)
    main_creek_coords = extract_main_creek(acc)
    heights = [Z[c] for c in main_creek_coords]
    # accumulation = [acc[c] for c in main_creek_coords]
    ratios = diff_ratios(heights, step_size)
    selected_indices = select_trenches(ratios, num_trenches)
    selected_coords = np.array(main_creek_coords)[selected_indices]
    return selected_coords

def water_accumulation(Z):
    grid = dem_to_grid(Z)

    # Conditioning terrain for water flow
    grid.fill_depressions(data='dem', out_name='flooded_dem')
    grid.resolve_flats(data='flooded_dem', out_name='inflated_dem')

    grid.flowdir(data='inflated_dem', out_name='dir')
    grid.accumulation(data='dir', out_name='acc')
    acc = grid.view('acc')

    # Important: No gaussian filter!!!

    return acc


def obtain_rank(data):
    sort_data = sorted(range(len(data)),key=lambda i:data[i],reverse=True)
    result = [0]*len(data)
    for i,idx in enumerate(sort_data,1):
        result[idx] = i
    return result

def select_trenches(ratios, num_trenches=1):
    """Partitions the ratios into num_trenches sections and finds the max in each section"""
    sections = np.array_split(ratios, num_trenches)
    max_per_section = [np.argmax(s) for s in sections]
    cumulative_lengths = np.insert(np.cumsum([len(s) for s in sections][:-1]), 0, 0)
    indices = cumulative_lengths + max_per_section
    return indices.astype(int)


neighbours = np.array([[-1,-1],[0,-1],[1,-1],[-1,0],[1,0],[-1,1],[0,1],[1,1]])
def extract_main_creek(acc):
    """returns all coordinates belonging to the creek with highest accumulation"""
    max_coord = np.unravel_index(np.argmax(acc), acc.shape)
    main_creek_coords = []
    while True:
        main_creek_coords.append(max_coord)
        adj_cells = [max_coord + n for n in neighbours]
        # remove coords outside boundary
        adj_cells = [tuple(a) for a in adj_cells if a[0] >= 0 and a[1] >= 0 and a[0] < acc.shape[0] and a[1] < acc.shape[1]]
        # remove coords with higher acc than current cell (only want to go up the creek, not back down)
        adj_cells = [a for a in adj_cells if acc[a] < acc[max_coord]]
        if not adj_cells:
            break
        adj_accs = [acc[a] for a in adj_cells]
        max_coord = adj_cells[np.argmax(adj_accs)]
    return main_creek_coords


def diff_ratios(heights, n=1):
    """Calculates the ratio in height differences used for identifying step terrain"""
    diffs = np.empty(len(heights) - n, dtype=int)
    for i in range(len(heights) - n):
        diffs[i] = heights[i + n] - heights[i]
    diffs = (diffs - min(diffs)) + 1  # Avoids issues with dividing by 0
    ratios = diffs[:-1] / diffs[1:]
    # Correction because diffs and ratios each truncate the array
    # Ideally I think we shouldn't be truncating though.
    diff_correction = [0] * n
    ratio_correction = [0]
    corrected = np.concatenate((diff_correction, ratios, ratio_correction))
    return corrected



