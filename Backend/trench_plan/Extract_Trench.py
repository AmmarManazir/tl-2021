import numpy as np
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon

def manhattan_dist(trench_cell, contour_cell):
    return np.linalg.norm(trench_cell - contour_cell)

def extract_trench(Grid, dict_Contours, cell =  [420, 300]):
    cell_x, cell_y = cell[0], cell[1]
    cell_height = Grid[cell_x, cell_y]
    contours_at_height = dict_Contours[str(int(cell_height))]
    point = Point(cell[0], cell[1])
    for contour in contours_at_height:
        contour_polygon = Polygon(contour.tolist())
        if contour_polygon.contains(point) or cell in contour.astype(np.uint64):
            return contour

    ####contingencies that we need sometimes, still pretty fast####
    ### when the point lies between 2 contours
    dists = []
    possible_trenches = []
    height_range = list(range(int(cell_height - 5), int(cell_height + 5)))
    for height in height_range:
        if dict_Contours[str(height)]:
            contours_at_height = dict_Contours[str(height)]
        for contour in contours_at_height:
                if cell in contour.astype(np.uint64):
                    return contour
                if Polygon(contour).contains(point):
                    possible_trenches.append(contour)
                    dists.append(manhattan_dist(np.array(cell), contour.mean(axis = 0)))
    if len(possible_trenches) > 0:
        return possible_trenches[np.argmin(np.array(dists))]
    return None



