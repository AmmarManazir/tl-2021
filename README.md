# Civilise.AI Techlauncher 2021 Landing Page

## Project Summary
Our project is an online tool to give professional-level land management advice to all rural landowners in the ACT NSW region.

We are achieving this goal by simulating models of entire landscapes, by utilising a combination of machine learning and bespoke algorithmic techniques.

This allows us to optimise groundwater levels, manage erosion, generate efficient paddock plans and tree planting guides tailor made to a specific property.

This will help landowners to better take care of their land to preserve both natural biodiversity and commercial output for generations to come.

Last year's team developed [this website](https://enjinwebsite.herokuapp.com/index). We are going to greatly improve on this in 2021.

## Codebase
We are using gitlab to develop code collaboratively. Currently the codebase is all from 2020, we will make it obvious which code we have written in 2021 as the project progresses. The code structure is as follows:

[Backend](Backend)
- Algorithms written in python to analyse the landscapes

[Frontend](Frontend)
- Code written in html, css, javascript and some python to visualise the landscapes
- We are using the Flask framework to communicate between the frontend and backend

[Demos](Demos)
- This is used for testing, demonstrations and explanations, but is not required for production.


## Project Documentation
We are using [google drive](https://drive.google.com/drive/folders/1xJXiuPo0l_YHa-qVCW2JOHOGDgGNUica?usp=sharing) to manage our project documentation. This includes:

Project Definition
- [Statement of Work](https://docs.google.com/document/d/1PGj-1mo_lCIW8GFEE10422AOVZf7kiJw0nAviurSe7I/edit)
- [Scope](https://docs.google.com/document/d/17DkohPeOkGnYDQ0jwPya15gSEGpVPZWnh5ktyQI-cHI/edit?usp=sharing)

Audits
- [Audit 1 slides](https://docs.google.com/presentation/d/1ZkTdzKqEiqwX6akAf5KbLFffKG4oySeLPUd_cVNzGdI/edit?usp=sharing)

Minutes
- [05-03-2021](https://docs.google.com/document/d/1kKlOoP21T60ocnHBzWuIyEV9joO4FJXdgo3HVoxB_sg/edit?usp=sharing)
- [09-03-2021](https://docs.google.com/document/d/1245IwltajPAyTtxSfSYPd6EGA0dI6SGuGjT2Yg2M1ZE/edit?usp=sharing)
- [10-03-2021](https://docs.google.com/document/d/1GgTkfWGYYGAQqQN875Y5wdH4l5FW2h0s-iWYbCdfAdA/edit?usp=sharing)
- [12-03-2021](https://docs.google.com/document/d/1sBe1JlIUmjutMM5jsbSe_0cf_qao5rQWmS4CEop493c/edit?usp=sharing)

Other
- [Decision Log](https://docs.google.com/spreadsheets/d/1UZZYfxHM9E23oYzX5pNOVE8-pUFiqjfMVPhKNyY9Y-M/edit?usp=sharing)
- [Risk Register](https://docs.google.com/spreadsheets/d/1VWVtTQ3iJ2JKHntTQ2qDvVy8Z_i1DI_9zmC1lyRdnp0/edit?usp=sharing)

