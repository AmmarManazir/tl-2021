## Setup Instructions

### Cloning the repo
- To clone the repo:  
    `git clone https://gitlab.com/civilise-ai/tl-2021.git`

### Conda environment
We recommend conda for managing your environments. If you don't already have it, you can [download conda here](https://docs.conda.io/en/latest/miniconda.html), and here's a nice [conda cheatsheet](https://conda.io/projects/conda/en/latest/_downloads/843d9e0198f2a193a3484886fa28163c/conda-cheatsheet.pdf). You can check if you have conda with `conda --version`.

- To create your environment:  
    `conda env create -f enjin_env.yml`
    
- To use your environment:  
    `conda activate enjin`  
    
- And finally, to check everything is working:  
    `python3 Frontend/app.py`

This should give you a link to play with the model in your browser.


### (Optional) Python virtual environment
If you don't like conda, another option is to use a [python virtual environment](https://docs.python.org/3/tutorial/venv.html). If you don't already have python, you can find the [latest version of python here](https://www.python.org/downloads/). Our code should support any version 3.7+.

- To create your environment:  
  `python3 -m venv enjin`
   
- To install the dependencies:  
    `source enjin/bin/activate`  
    `pip install -r requirements.txt`
  
- And finally, to check everything is working:  
    `python3 Frontend/app.py`

This should give you a link to play with the model in your browser.

### IDE
I use [Pycharm](https://www.jetbrains.com/pycharm/download/#section=mac). As an ANU student, you can get [Pycharm Professional for free](https://www.jetbrains.com/community/education/#students). But you're welcome to use [VSCode](https://code.visualstudio.com/), or [Spyder](https://www.spyder-ide.org/), or whatever else you prefer.
